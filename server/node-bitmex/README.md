# BitMEX API Node.js Connector (Simplified)

This project is a simplified version of the official [BitMEX API Node.js Connector](https://github.com/BitMEX/api-connectors/tree/master/official-http/node-swagger).

### Prerequisites

* [Node.js](https://nodejs.org/en/) 10.0.0 or higher
* [Yarn](https://yarnpkg.com/en/) 1.1.0 or higher

### Installing

* Clone the project into a directory of your choice
* Run the "yarn" command inside the project folder.

```bash
git clone https://github.com/EnosDomingues/node-bitmex.git
cd node-bitmex
yarn
```

## Running the tests

Put your api-key and secret in the app.js file

```javascript
const key = ''; 
const secret = '';
```

Check in the app.js file if only the method "client.getMargin ();" is uncommented and run app.js

```bash
node app.js
```

## Considerations

* The project is still under development and may contain bugs.
* The app.js file contains several examples of how to interact with bitmex.
* Always remember to keep commented or delete any code that will not be used.
