const SwaggerClient = require("swagger-client");
const BitMEXAPIKeyAuthorization = require('./lib/BitMEXAPIKeyAuthorization');

const manager = new SwaggerClient({
  url: 'https://www.bitmex.com/api/explorer/swagger.json',
  usePromise: true
});

const globals = '../common/globals.js';

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

let currentOrders = [{}];
let ordStatus = [{}];
let orderQty = [{}];
let timeStamp = [{}];
let side = [{}];
let price = [{}];

class Client {

  constructor(key, secret) {
    this.authorizeUser(key, secret); 
  }

  authorizeUser(key, secret) {
    manager
      .then(async function (client) {
        client.clientAuthorizations.add("apiKey", new BitMEXAPIKeyAuthorization(key, secret));
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /*  Announcement : Public Announcements */

  announcement() {
    manager
      .then(function (client) {
        client.Announcement.Announcement_get({ count: 1, reverse: true})
          .then(function (response) {
            const announcements  = JSON.parse(response.data);
            console.log(announcements);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  announcemenUrgent() {
    manager
      .then(function (client) {
        client.Announcement.Announcement_getUrgent()
          .then(function (response) {
            const announcements  = JSON.parse(response.data);
            console.log(announcements);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  /* APIKey : Persistent API Keys for Developers */

  getApiKey() {
    manager
      .then(function (client) {
        client.APIKey.APIKey_get({ reverse: true})
          .then(function (response) {
            const apiKeys  = JSON.parse(response.data);
            console.log(apiKeys);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  /* Chat : Trollbox Data */

  chatMessages() {
    manager
      .then(function (client) {
        client.Chat.Chat_get({ count: 1, reverse: true, channelID:1 })
          .then(function (response) {
            const messages  = JSON.parse(response.data);
            console.log(messages);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  chatNew(message, channelID) {
    manager
      .then(function (client) {
        client.Chat.Chat_new({ message: message, channelID:channelID })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  chatChannnels() {
    manager
      .then(function (client) {
        client.Chat.Chat_getChannels()
          .then(function (response) {
            const channels  = JSON.parse(response.data);
            console.log(channels);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  chatConnected() {
    manager
      .then(function (client) {
        client.Chat.Chat_getConnected()
          .then(function (response) {
            const connected  = JSON.parse(response.data);
            console.log(connected);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  /* Execution : Raw Order and Balance Data */

  execution(count) {
    manager
      .then(function (client) {
        client.Execution.Execution_get({reverse: true, count:count})
          .then(function (response) {
            const executions  = JSON.parse(response.data);
            console.log(executions);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  executionTradeHistory(count) {
    manager
      .then(function (client) {
        client.Execution.Execution_getTradeHistory({reverse: true, count:count})
          .then(function (response) {
            const executions  = JSON.parse(response.data);
            console.log(executions);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* Funding : Swap Funding History */

  funding(count) {
    manager
      .then(function (client) {
        client.Funding.Funding_get({reverse: true, count:count, symbol: "XBT:perpetual"})
          .then(function (response) {
            const executions  = JSON.parse(response.data);
            console.log(executions);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* GlobalNotification : Account Notifications */

  globalNotifications() {
    manager
      .then(function (client) {
        client.GlobalNotification.GlobalNotification_get()
          .then(function (response) {
            const notifications  = JSON.parse(response.data);
            console.log(notifications);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* Instrument : Tradeable Contracts, Indices, and History */

  instrument() {
    manager
      .then(function (client) {
        client.Instrument.Instrument_get({ symbol: "XBT:perpetual" })
          .then(function (response) {
            const instruments  = JSON.parse(response.data);
            console.log(instruments[0].askPrice, instruments[0].bidPrice );
            localStorage.setItem("askPrice", instruments[0].askPrice);
            localStorage.setItem("bidPrice", instruments[0].bidPrice);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unab--le to connect:", e);
      })
  }

  instrumentActive() {
    manager
      .then(function (client) {
        client.Instrument.Instrument_getActive()
          .then(function (response) {
            const instruments  = JSON.parse(response.data);
            console.log(instruments);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  instrumentIndices() {
    manager
      .then(function (client) {
        client.Instrument.Instrument_getIndices()
          .then(function (response) {
            const instruments  = JSON.parse(response.data);
            console.log(instruments);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  instrumentActiveAndIndices() {
    manager
      .then(function (client) {
        client.Instrument.Instrument_getActiveAndIndices()
          .then(function (response) {
            const instruments  = JSON.parse(response.data);
            console.log(instruments);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  instrumentActiveIntervals() {
    manager
      .then(function (client) {
        client.Instrument.Instrument_getActiveIntervals()
          .then(function (response) {
            const instruments  = JSON.parse(response.data);
            console.log(instruments);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  instrumentCompositeIndex() {
    manager
      .then(function (client) {
        client.Instrument.Instrument_getCompositeIndex({ symbol:".XBT"})
          .then(function (response) {
            const instruments  = JSON.parse(response.data);
            console.log(instruments);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* Insurance : Insurance Fund Data */

  insurance(count) {
    manager
      .then(function (client) {
        client.Insurance.Insurance_get({ count: count, reverse: true})
          .then(function (response) {
            const insurance  = JSON.parse(response.data);
            console.log(insurance);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* Leaderboard : Information on Top Users */

  leaderboard() {
    manager
      .then(function (client) {
        client.Leaderboard.Leaderboard_get()
          .then(function (response) {
            const leaderboard  = JSON.parse(response.data);
            console.log(leaderboard);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  leaderboardName() {
    manager
      .then(function (client) {
        client.Leaderboard.Leaderboard_getName()
          .then(function (response) {
            const leaderboard  = JSON.parse(response.data);
            console.log(leaderboard);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* Liquidation : Active Liquidations */

  liquidation() {
    manager
      .then(function (client) {
        client.Liquidation.Liquidation_get()
          .then(function (response) {
            const liquidation  = JSON.parse(response.data);
            console.log(liquidation);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* Order : Placement, Cancellation, Amending, and History */

  getOrders(open) {
    console.log('gettingoroders');
    currentOrders = [];
    ordStatus = [];
    manager
      .then(function (client) {
        client.Order.Order_getOrders({ filter: JSON.stringify({ "open": open }) })
          .then(function (response) {
            const orders  = JSON.parse(response.data);
            if(globals.primaryOrderID ===''){
              if(orders[0]){
                console.log('ready to aassign primaryOrderID')
              }
            }
            var t3 = 0;
            orders.forEach(element => { 
              console.log(' NEVER SHOWS ',element.orderID, element.price);
              currentOrders[t3]=element.orderID;
              ordStatus[t3]=element.ordStatus;
              orderQty[t3]=element.orderQty;
              timeStamp[t3]=element.timestamp;
              side[t3]=element.side;
              price[t3]=parseInt(element.price);
              t3 = t3 +1;
            }); 
            console.log('price from CLient.js -- ', price[0])
            localStorage.setItem("currentOrders", currentOrders[0])
            localStorage.setItem("currentOrdStatus", ordStatus[0])
            localStorage.setItem("currentOrderQty", orderQty[0])
            localStorage.setItem("currentOrderTimestamp", timeStamp[0])
            localStorage.setItem("currentOrderSide", side[0])
            localStorage.setItem("currentOrderPrice", price[0])
          //  console.log(orders[0].orderID);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
  }

  updateOrder(orderID, orderQty, price) {
    manager
      .then(async function (client) {
        await client.Order.Order_amend({
          orderID: orderID,
          orderQty: orderQty,
          price: price,
        });
      })
      .catch(function (e) {
        console.error('Unable to connect:', e);
      })
  }

  createOrder(price, orderQty) {
    manager
      .then(async function (client) {
        await client.Order.Order_new({ symbol: 'XBTUSD', price: price, orderQty: orderQty })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  cancelOrder(orderID) {
    manager
      .then(async function (client) {
        await client.Order.Order_cancel({ orderID: orderID })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  cancelAllOrders() {
    manager
      .then(async function (client) {
        await client.Order.Order_cancelAll()
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  cancelAllOrdersAfter(ms) {
    manager
      .then(async function (client) {
        await client.Order.Order_cancelAllAfter({ timeout: ms })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  bulkOrders(orders) {
    manager
      .then(async function (client) {
        await client.Order.Order_newBulk({ orders: JSON.stringify(orders) })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  bulkOrdersUpdate(orders) {
    manager
      .then(async function (client) {
        await client.Order.Order_amendBulk({ orders: JSON.stringify(orders) })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  closePosition(orders) {
    manager
      .then(async function (client) {
        await client.Order.Order_closePosition({ symbol: "XBTUSD", price: 8000 })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

  /* OrderBook : Level 2 Book Data */

  orderBook() {
    manager
      .then(function (client) {
        client.OrderBook.OrderBook_getL2({ symbol: "XBTUSD", depth: 25 })
          .then(function (response) {
            const orderBook  = JSON.parse(response.data);
            console.log(orderBook);
          })
          .catch(function (e) {
            console.log('Error:', e.statusText);
          })
      })
      .catch(function (e) {
        console.error("Unable to connect:", e);
      })
  }

    /* Position : Summary of Open and Closed Positions */

    position() {
      manager
        .then(function (client) {
          client.Position.Position_get({ "symbol": "XBTUSD" })
            .then(function (response) {
              const positions  = JSON.parse(response.data);
              var t1 =positions[0].currentQty;
              console.log(typeof t1, t1)
              localStorage.setItem("qty", t1)
            })
            .catch(function (e) {
              console.log('Error pos:', e.statusText);
            })
        })
        .catch(function (e) {
          console.error("Unable to connect:", e);
        })
    }


    positionIsolateMargin() {
        manager
        .then(async function (client) {
          client.Position.Position_isolateMargin({ "symbol": "XBTUSD", enabled: false });
        })
        .catch(function (e) {
          console.error("Unable to connect:", e);
        })
    }

    /* User : Account Operations */

    getMargin() {
      manager
        .then(function (client) {
          client.User.User_getMargin()
            .then(function (response) {
              const margin  = JSON.parse(response.data);
              const marginBalance = (margin.marginBalance / 1e8).toFixed(4);
              console.log("\nMargin Balance:", marginBalance, "XBT");
            })
            .catch(function (e) {
              console.log('Error:', e.statusText);
            })
        })
        .catch(function (e) {
          console.error("Unable to connect:", e);
        })
    }

}

module.exports = Client;




