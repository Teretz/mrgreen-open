const Client = require('./Client');

const key = ''; 
const secret = '';

const client = new Client(key, secret);

/*  Announcement : Public Announcements */

// client.announcement();
// client.announcemenUrgent();

/* APIKey : Persistent API Keys for Developers */

// client.getApiKey();

/* Chat : Trollbox Data */

// client.chatMessages();
// client.chatNew("Hello", 1);
// client.chatChannnels();
// client.chatConnected();

/* Execution : Raw Order and Balance Data */

// client.execution(1);
// client.executionTradeHistory(1);

/* Funding : Swap Funding History */

// client.funding(10);

/* GlobalNotification : Account Notifications */
// This is an upcoming feature and currently does not return data.

// client.globalNotifications();

/* Instrument : Tradeable Contracts, Indices, and History */

// client.instrument();
// client.instrumentActive();
// client.instrumentIndices();
// client.instrumentActiveAndIndices();
// client.instrumentActiveIntervals();
// client.instrumentCompositeIndex();

/* Insurance : Insurance Fund Data */

// client.insurance(5);

/* Leaderboard : Information on Top Users */

// client.leaderboard();
// client.leaderboardName();

/* Liquidation : Active Liquidations */

// client.liquidation();

/* Order : Placement, Cancellation, Amending, and History */

// const orders = 
//   [
//     {symbol: "XBTUSD", orderQty: 20, ordType: "Limit", side: "Buy", price: 6990},
//     {symbol: "XBTUSD", orderQty: 25, ordType: "Limit", side: "Buy", price: 6980},
//     {symbol: "XBTUSD", orderQty: 30, ordType: "Limit", side: "Buy", price: 6970},
//     {symbol: "XBTUSD", orderQty: 35, ordType: "Limit", side: "Buy", price: 6960},
//     {symbol: "XBTUSD", orderQty: 40, ordType: "Limit", side: "Buy", price: 6950},
//   ];

// const ordersToUpdate = 
//   [
//     {orderID: "42ed502a-1835-ed9f-aa68-1300b53f0212", symbol: "XBTUSD", orderQty: 20, ordType: "Limit", side: "Buy", price: 5990 },
//     {orderID: "4f83dd82-2fb4-cd06-c04c-e7637dad95ed", symbol: "XBTUSD", orderQty: 20, ordType: "Limit", side: "Buy", price: 5980 },
//     {orderID: "247a777a-e632-d4b0-6ced-090cbac77163", symbol: "XBTUSD", orderQty: 20, ordType: "Limit", side: "Buy", price: 5970 },
//     {orderID: "55872360-6bbf-7356-9b38-665b03479a0d", symbol: "XBTUSD", orderQty: 20, ordType: "Limit", side: "Buy", price: 5960 },
//     {orderID: "7c12b967-41c2-8da2-637f-57364a166195", symbol: "XBTUSD", orderQty: 20, ordType: "Limit", side: "Buy", price: 5950 },
//   ];

// client.bulkOrders(orders);
// client.bulkOrdersUpdate(ordersToUpdate);
// client.closePosition();
// client.getOrders(true);
// client.updateOrder("83898981-62d9-8564-64e0-9f53f4b2ebf4", 50, 7090);
// client.createOrder(7200, 50);
// client.cancelOrder("c9cdd578-1a64-f5b0-a59c-a6a80e2de8ef");
// client.cancelAllOrders();
// client.cancelAllOrdersAfter(60000);

/* OrderBook : Level 2 Book Data */

// client.orderBook();

/* Position : Summary of Open and Closed Positions */

// client.position();
// client.positionIsolateMargin();

/* Quote : Best Bid/Offer Snapshots & Historical Bins */

/* Schema : Dynamic Schemata for Developers */

/* Settlement : Historical Settlement Data */

/* Stats : Exchange Statistics */

/* Trade : Individual & Bucketed Trades */

/* User : Account Operations */

client.getMargin();

/* UserEvent : User Events for auditing */