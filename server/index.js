import './common/env';
import Server from './common/server';
import routes from './routes';
import App from './appServer';

/*global.prevLSL = 222323;
  global.prevSSL = 222323;
  global.currentLSL = 222323;
  global.currentSSL = 222323;

global.positionToOpenWith = 15;

global.targetPosition = 30;
global.targetLongsPosition =30;
global.targetShortsPosition = 30;
global.targetLSLPosition =30;
global.targetSSLPosition = 30;
*/

export default new Server().router(routes).listen(process.env.PORT);
