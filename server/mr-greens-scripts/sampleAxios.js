import axios from 'axios';
import express from 'express';
import curlirize from 'axios-curlirize';
 
const app = express();
 
// initializing axios-curlirize with your axios instance
curlirize(axios);
 
// creating dummy route
app.post('/', (req, res) => {
  res.send({ hello: 'world!' });
});
 
// starting server
app.listen(7500, () => {
  console.log('Dummy server started on port 7500');
  /*
             The output of this in the console will be :
             curl -X POST -H "Content-Type:application/x-www-form-urlencoded" --data {"dummy":"data"} http://localhost:7500/
        */
  axios
    .post('http://localhost:7500/', { dummy: 'data' })
    .then(res => {
      console.log('success');
    })
    .catch(err => {
      console.log(err);
    });
});