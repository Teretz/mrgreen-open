//for adaptaive limit orders
module.exports = function doAdaptiveLimit(req, res) {
  var fs = require('fs');
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var sideUrl = req.query.side;
  var amountUrl = req.query.amount;
  //userRequests++;
  (async function main() {
    try {
      const shell = require('shelljs');

      console.log(
        'env BITMEX_API_KEY_ID=',
        apiKey,
        ' BITMEX_API_KEY_SECRET=',
        apiSecret,
        ' node index.js ',
        sideUrl,
        ' ',
        amountUrl
      );

      shell.exec(
        'env BITMEX_API_KEY_ID=' +
          apiKey +
          ' BITMEX_API_KEY_SECRET=' +
          apiSecret +
          ' node index.js ' +
          sideUrl +
          ' ' +
          amountUrl
      );

      console.log('adaptiveLimit - - - - ');

      res.send({ apiKey });
    } catch (e) {
      console.error(e);
    }
  })();
  console.log(userRequests, 'Did not honor interval.	closePosition				');
};

module.exports = function fetchClosingIndicators() {
  console.log('fetching closing inidcators from functions.js');
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var fs = require('fs');
  (async function main() {
    try {
      const shell = require('shelljs');
      shell.exec(
        'curl -X GET "https://mrgreen-scratch.getsandbox.com/v2/indicator/byType/Closing"'
      );
    } catch (e) {
      console.error(e);
    }
  })();
};

module.exports = function fetchTrendingIndicators() {
  console.log('fetching trend inidcators');
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var fs = require('fs');
  (async function main() {
    try {
      const shell = require('shelljs');
      shell.exec(
        'curl -X GET "https://mrgreen-scratch.getsandbox.com/teretz/scratch/1.0.0/indic' +
          'ator/findByStatus?status=trend" -H "accept: application/json"'
      );
    } catch (e) {
      console.error(e);
    }
  })();
};

module.exports = function fetchMomentumIndicators() {
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var fs = require('fs');
  (async function main() {
    try {
      const shell = require('shelljs');
      shell.exec(
        'curl -X GET "https://mrgreen-scratch.getsandbox.com/teretz/scratch/1.0.0/indic' +
          'ator/findByStatus?status=momentum" -H "accept: application/json"'
      );
    } catch (e) {
      console.error(e);
    }
  })();
};

module.exports = function doAdaptiveLimit(req, res) {
  var fs = require('fs');
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var sideUrl = req.query.side;
  var amountUrl = req.query.amount;
  //userRequests =;
  (async function main() {
    try {
      const shell = require('shelljs');
      console.log(
        'env BITMEX_API_KEY_ID=',
        apiKey,
        ' BITMEX_API_KEY_SECRET=',
        apiSecret,
        ' node index.js ',
        sideUrl,
        ' ',
        amountUrl
      );
      shell.exec(
        'env BITMEX_API_KEY_ID=' +
          apiKey +
          ' BITMEX_API_KEY_SECRET=' +
          apiSecret +
          ' n' +
          'ode index.js ' +
          sideUrl +
          ' ' +
          amountUrl
      );
      console.log('adaptiveLimit - - - - ');
      res.send({ apiKey });
    } catch (e) {
      console.error(e);
    }
  })() + 1;
  console.log('Did not honor interval.	closePosition				');
};

module.exports = function doPlaceOrder() {
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var symbolUrl = req.query.symbol;
  var orderQtyUrl = req.query.orderQty;
  var priceUrl = req.query.price;
  var ordTypeUrl = req.query.ordType;
  var execInstUrl = req.query.execInst;
  var clOrdIdUrl = req.query.clOrdId;
  userRequests++;
  // res.send({ express: 'Hello. This page shows an order of ' + orderQtyUrl + ' '
  // + symbolUrl + ' was placed for ' + priceUrl + ' price with ' + ordTypeUrl + '
  // as the order type. '   });

  if (num_requests < request_limit) {
    num_requests = num_requests - 1;
    throttle(function() {
      var rpms = calculateRPMS(++num_requests, time_started);

      (async function main() {
        try {
          /*const result = await makeRequest('GET', 'position', {
                  filter: { symbol: 'XBTUSD' },
                  columns: ['currentQty', 'avgEntryPrice'],
                });*/
          const result = await makeRequest(apiKey, apiSecret, 'POST', 'order', {
            symbol: symbolUrl,
            orderQty: orderQtyUrl,
            price: priceUrl,
            ordType: ordTypeUrl,
            clOrdId: clOrdIdUrl,
          });
          //console.log(rpms, max_rpms, result);
          formatTime();
          console.log(
            userRequests,
            'placeOrder',
            formattedTime,
            num_requests,
            rpms,
            max_rpms
          );

          res.send({ express: result });
        } catch (e) {
          console.error(e);
        }
      })();

      if (rpms > max_rpms) {
        console.log(userRequests, 'Did not honor interval.	placeOrder				', rpms);
      }
    });
  }
};

module.exports = function makeRequest(
  apiKeyPassed,
  apiSecretPassed,
  verb,
  endpoint,
  data = {}
) {
  const apiRoot = '/api/v1/';
  var apiKey = apiKeyPassed;
  var apiSecret = apiSecretPassed;
  const expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future

  let query = '',
    postBody = '';
  if (verb === 'GET') query = '?' + qs.stringify(data);
  // Pre-compute the reqBody so we can be sure that we're using *exactly* the same body in the request
  // and in the signature. If you don't do this, you might get differently-sorted keys and blow the signature.
  else postBody = JSON.stringify(data);

  const signature = crypto
    .createHmac('sha256', apiSecret)
    .update(verb + apiRoot + endpoint + query + expires + postBody)
    .digest('hex');

  const headers = {
    'content-type': 'application/json',
    accept: 'application/json',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKey,
    'api-signature': signature,
  };

  const requestOptions = {
    method: verb,
    headers,
  };
  if (verb !== 'GET') requestOptions.body = postBody; // GET/HEAD requests can't have body
  const url = 'http://testnet.bitmex.com' + apiRoot + endpoint + query;

  return fetch(url, requestOptions)
    .then(response => response.json())
    .then(
      response => {
        //num_requests = num_requests -1;
        if ('error' in response) throw new Error(response.error.message);
        return response;
      },
      error => console.error('Network error', error)
    );
};

module.exports = function commitToDB(req, res) {
  var mongoose = require('mongoose');
  var dbUri = 'mongodb://localhost:27017/api';
  var dbConnection = mongoose.createConnection(dbUri);
  var Schema = mongoose.Schema;
  var postSchema = new Schema({
    title: String,
    text: String,
  });
  var Post = dbConnection.model('Post', postSchema, 'posts');
  Post.find({}, function(error, posts) {
    console.log(posts);
    process.exit(1);
  });
};
