const Client = require('../node-bitmex/Client');
const fs = require('fs');
let env = require('dotenv');
let qty = 0;
let t1 = 0;
let side = 'none';

let _side = 'none';

const bodyParser = require('body-parser');
const path = require('path');
var request = require('request');

let localStorage = null;
if (typeof localStorage === 'undefined' || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

//how many targets
const targets = 5;

// 1 is prefix as we want our investment; ex: 1.0 plus 0.004 growth = ( 1.0 + 0.004) = 1.004
// we then divide/multiply, depending on short/long, the target by current price in order to
// get the price placed for each target;
const target1 = 1.004;
const target2 = 1.005;
const target3 = 1.006;
const target4 = 1.007;
const target5 = 1.008;

let target1Price = 0;
let target2Price = 0;
let target3Price = 0;
let target4Price = 0;
let target5Price = 0;

let apiKey = '';
let apiSecret = '';
let currentSignal = '';

module.exports = function(sideParam) {
  apiKey = localStorage.getItem('apiKey');
  apiSecret = localStorage.getItem('apiSecret');
  //console.log(apiKey)
  //this.apiKey = apiKey;
  //this.apiSecret = apiSecret;
  const client = new Client(apiKey, apiSecret);
  let currentSignal = localStorage.getItem('latestSignal');
  _side = currentSignal;

  let orders = [
    {
      symbol: 'XBTUSD',
      orderQty: t1,
      ordType: 'Limit',
      side: _side,
      price: target1Price,
    },
    {
      symbol: 'XBTUSD',
      orderQty: t1,
      ordType: 'Limit',
      side: _side,
      price: target2Price,
    },
    {
      symbol: 'XBTUSD',
      orderQty: t1,
      ordType: 'Limit',
      side: _side,
      price: target3Price,
    },
    {
      symbol: 'XBTUSD',
      orderQty: t1,
      ordType: 'Limit',
      side: _side,
      price: target4Price,
    },
    {
      symbol: 'XBTUSD',
      orderQty: t1,
      ordType: 'Limit',
      side: _side,
      price: target5Price,
    },
  ];
};

const getTarget1 = () => {
  return target1;
};

const getTarget2 = () => {
  return target2;
};
const getTarget3 = () => {
  return target3;
};
const getTarget4 = () => {
  return target4;
};
const getTarget5 = () => {
  return target5;
};

function deployTargets() {
  console.log(' YUP YUP  YYUP  TTYUURTYRUYRU', currentSignal);
  qty = localStorage.getItem('currentOrderQty');

  if (targets > 0) {
    t1 = qty / targets;

    if (currentSignal === 'Buy') {
      _side = 'Long';
    }

    if (currentSignal === 'Sell') {
      _side = 'Short';
    }
  }
  console.log('kkkkkkkkkkkkkkkkkkkkkkkkkkk');
  return client.bulkOrders(orders);
}

exports.target1 = target1;
exports.target2 = target2;
exports.target3 = target3;
exports.target4 = target4;
exports.target5 = target5;

exports.getTarget1 = getTarget1;
exports.getTarget2 = getTarget2;
exports.getTarget3 = getTarget3;
exports.getTarget4 = getTarget4;
exports.getTarget5 = getTarget5;

exports.deployTargets = deployTargets;
