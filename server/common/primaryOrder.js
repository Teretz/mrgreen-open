// Modules
const SwaggerClient = require('swagger-client');
const assert = require('assert');
const BitMEXAPIKeyAuthorization = require('../common/lib/BitMEXAPIKeyAuthorization');
const BitMEXWSClient = require('bitmex-realtime-api');
const fs = require('fs');
import { BitMexPlus } from 'bitmex-plus';
let env = require('dotenv')


let localStorage = null;

if (typeof localStorage === 'undefined' || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

const bodyParser = require('body-parser');
const path = require('path');
var request = require('request');

// Globals
const testnet = process.env.BITMEX_TESTNET === 'false';
const websitePrefix = testnet ? 'testnet' : 'www';
const symbol = process.env.BITMEX_CONTRACT || 'XBTUSD';

//const Client = require(process.env.NODEBITMEX_CLIENT_PO);
const Client = require('../node-bitmex/Client') 

//console.log(cwd())

module.exports = function(sideParam) {

    this.apiKey = process.env.API_KEY;
    this.apiSecret = process.env.API_SECRET;
    this.qty = localStorage.getItem('currentOrderQty');
    this.sideParam = sideParam;
    
    console.log('lplplplplplplplplplplplplplplp', this.apiKey, this.apiSecret, this.qty, this.sideParam)
    
  
  //  this.fullName = function() {
  //    return this.firstName + ' ' + this.lastName;
  //  };
  
    // Command line: two args, first is Buy or Sell, second is number of contracts.
   // assert(process.argv.length == 4);
  
    const capitalizeFirstLetter = s =>
      s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
    const side = this.sideParam;
    const orderQty = this.qty;
   // console.log(orderQty);
  



    // 1s delayer/waiter to avoid api request limit.
    let needToWait = false;
    let waitPeriodTimeout;
    const requestWaitPeriod = () => {
      needToWait = true;
      if (waitPeriodTimeout) clearTimeout(waitPeriodTimeout);
      waitPeriodTimeout = setTimeout(() => {
        needToWait = false;
        console.log('.');
        amendPrice();
      }, 1000);
    };
  
    let lastPrice;
      //  if (isFirstPrice) {
      //    resolve(lastPrice);
      //    isFirstPrice = false;
      //  } else {
      //    amendPrice();
      //  }
      let askPrice = localStorage.getItem("askPrice");
      let bidPrice = localStorage.getItem("bidPrice");

      if (side == 'Buy') lastPrice = parseFloat( askPrice ) - 0.5;
      if (side == 'Sell')  lastPrice = parseFloat( bidPrice ) + 0.5;


  // Create initial order.
  let orderID;
  let currentOrderPrice;


  currentOrderPrice = lastPrice;
  //const client = await clientPromise;
  const client = new Client(this.apiKey, this.apiSecret);
  orderID = localStorage.getItem("primaryOrderID");
  //await client.Order.Order_amend({ orderID, price: lastPrice }) 
  console.log('++++++++++++++++', this.apiKey, this.apiSecret, orderID, orderQty, lastPrice)

  client.updateOrder(orderID, orderQty, lastPrice);
 
  


}


/*

  const clientPromise = new SwaggerClient({
    url: `https://${websitePrefix}.bitmex.com/api/explorer/swagger.json`,
    usePromise: true,
  });
  clientPromise
    .then(async client => {
      client.clientAuthorizations.add(
        'apiKey',
        new BitMEXAPIKeyAuthorization(
          this.apiKey,
          this.apiSecret
        )
      );

      currentOrderPrice = await firstPrice;
      console.log(`Creating order with price: ${currentOrderPrice}`);
    /*  orderID = (
        await client.Order.Order_new({
          symbol,
          currentOrderPrice,
          orderQty,
          side,
          execInsts: 'ParticipateDoNotInitiate',
        })
      ).obj.orderID;
    

     orderID = localStorage.getItem("primaryOrderID");
     amendPrice();
    })
    .catch(e =>
      //console.error(`Error (unable to connect?): ${e.errObj.message}`)
      console.error(`Error (unable to connect?): ${e}`)
    );

  const amendPrice = async () => {
    if (needToWait) return;
    needToWait = true;


      currentOrderPrice = lastPrice;
      //const client = await clientPromise;
      const client = new Client(this.apiKey, this.apiSecret);
      orderID = localStorage.getItem("primaryOrderID");
      //await client.Order.Order_amend({ orderID, price: lastPrice })
      //console.log(this.apiKey, this.apiSecret, orderID, orderQty, lastPrice)
      if( process.env.PROCESS_ORDERS === true){
     return await client.updateOrder(orderID, orderQty, lastPrice)
       .then(() => {
          console.log(`Order amended with new price: ${currentOrderPrice}`);
          let data = {
            msg: `Order amended with new price: ${currentOrderPrice} as`,
            orderPrice: currentOrderPrice,
            orderId: orderID,
            status: 'waiting',
          };
        
        })
        .catch(e => {
          if (e.obj.error.message == 'Invalid ordStatus') {
            console.log(orderID, ' Order executed successfully.');
            process.exit(0);
          } else {
            console.error(e.message);
            process.exit(1);
          }
     
        })
      }

  }}

  */