let localStorage = null;

if (typeof localStorage === 'undefined' || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }

let p1 = localStorage.getItem("primaryOrderID");
let p2 = localStorage.getItem("primaryTargets");
let p3 = localStorage.getItem("updatePrimaryOrderID");
let p4 = localStorage.getItem("adjustPrimaryOrder");
let p5 = false;
let p6 = 0;
let p7 = 0;

exports.primaryOrderID = p1;
exports.primaryTargets = p2;
exports.updatePrimaryOrderID = p3;
exports.adjustPrimaryOrder = p4;
exports.adjustPrimaryTargets = p5;
exports.askPrice = p6;
exports.bidPrice = p7;

import dotenv from 'dotenv';

dotenv.config();

exports.message = function message(msg){
    this.msg = msg;
    return this.msg;
}

exports.setPrimaryOrderID = function setPrimaryOrderID(msg){
    this.msg = msg;
    localStorage.setItem("primaryOrderID", this.msg);
    p1 = this.msg;
    return this.msg;
}