


const BitMEXClient = require('bitmex-realtime-api');
const bodyParser = require('body-parser');
const path = require('path');
var request = require('request');

let key = process.env.API_KEY;
let secret = process.env.API_SECRET;
let localStorage = null;

if (typeof localStorage === 'undefined' || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

module.exports = function() {
    this.apiKey = process.env.API_KEY;
    this.apiSecret = process.env.API_SECRET;

    key = this.apiKey;
    secret = this.apiSecret;

    console.log('---------------------',key, secret)
// See 'options' reference below
const wsClient = new BitMEXClient({testnet: false,  apiKeyID: key, apiKeySecret: secret, maxTableLen: 10000});
// handle errors here. If no 'error' callback is attached. errors will crash the wsClient.
wsClient.on('error', console.error);
wsClient.on('open', () => console.log('Connection opened.'));
wsClient.on('close', () => console.log('Connection closed.'));
wsClient.on('initialize', () => console.log('wsClient initialized, data is flowing.'));



  wsClient.addStream('XBTUSD', 'position', function(data, symbol, tableName) {
    //console.log(`Got update for ${tableName}:${symbol}. Current state:\n${JSON.stringify(data).slice(0, 100)}...`);
   //console.log(JSON.stringify(data));
   if(  data[0].currentQty !==0 ){
    localStorage.setItem("currentQty", data[0].currentQty );
    localStorage.setItem("unrealisedPnl", data[0].unrealisedPnl );
    localStorage.setItem("unrealisedPnlPcnt", data[0].unrealisedPnlPcnt );
    localStorage.setItem("unrealisedRoePcnt", data[0].unrealisedRoePcnt );
   }
  });
  wsClient.addStream('XBTUSD', 'instrument', function(data, symbol, tableName) {
    //console.log(`Got update for ${tableName}:${symbol}. Current state:\n${JSON.stringify(data).slice(0, 100)}...`)
    localStorage.setItem("askPrice", data[0].askPrice );
    localStorage.setItem("bidPrice", data[0].bidPrice );
  });
  wsClient.addStream('XBTUSD', 'order', function(data, symbol, tableName) {
    console.log(`Got update for ${tableName}:${symbol}. Current state:\n${JSON.stringify(data).slice(0, 100)}...`);
   console.log(JSON.stringify(data));
    
    localStorage.setItem("currentOrders", data[0].orderID)
            localStorage.setItem("primaryOrderID", data[0].orderID)
            localStorage.setItem("currentOrdStatus", data[0].ordStatus)
            localStorage.setItem("currentOrderQty", data[0].orderQty)
            localStorage.setItem("currentOrderTimestamp", data[0].timestamp)
            localStorage.setItem("currentOrderSide", data[0].side)
            localStorage.setItem("currentOrderPrice", data[0].price)
  });


}

