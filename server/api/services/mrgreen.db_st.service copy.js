'use strict';

import { signedCookie } from 'cookie-parser';
import l from '../../common/logger';

require('util').inspect.defaultOptions.depth = null;

let halfsies;
let negativeHalfsies;
let signalCounter = 0;
let side;

let prevSignal = 'none';

var express = require('express'),
  app = express(),
  PouchDB = require('pouchdb');

app.use(express.json());
const axios = require('axios');
let localStorage = null;

//const adaptiveLimit = require('../../common/adaptiveLimit');
const adaptiveLimit = require('../../common/adapt');

PouchDB.plugin(require('pouchdb-find'));
app.use('/api/mrgreen', require('express-pouchdb')(PouchDB));

//let db = new PouchDB('supertrend');
var db_st = new PouchDB('supertrend');

//const Client = require(process.env.NODEBITMEX_CLIENT);
//const Client = require('/home/ubuntu/mr-greenswag/server/node-bitmex/Client');
const Client = require('../../node-bitmex/Client');

const key = 'ZLlv0N46ACT8PbZn7ban0zMF';
const secret = 'P6Z6cQwWPFnbM7vPp6Cu6LjHwIhNHjVpKP88HxGIOmy3lB4n';
const client = new Client(key, secret);

let testingOrdersize = 30;
let testing = true;
let currentQty = 0;
let qtyToFill = 0;
let currentSignal = 'Buy';
let orderPlaced = true;
let orderCounter = 0;
//for what value/pip to assign the loss at
let stoploss_target_short = 0;
let stoploss_target_long = 0;
let ssl = stoploss_target_short;
let lsl = stoploss_target_long;
let midprice = 0;
let stoploss_start_short = 0;
let stoploss_start_long = 0;

let stoploss_held_short = 0;
let stoploss_held_long = 0;

let stoploss_reversal_short = 0;
let stoploss_reversal_long = 0;

let stoploss_orderid_short = '';
let stoploss_orderid_long = '';

let stoplossReversal = false;

let stoploss_direction = 'none';

let supertrendbuylabel = 0;
let supertrendselllabel = 0;

let remoteCouch_st = 'http://localhost:5984/supertrend';

let prev_stoploss_target_long = 0;
let prev_stoploss_target_short = 0;
let current_stoploss_target_long = 0;
let current_stoploss_target_short = 0;

let latestSignal = 'Sell';
let latestTimeSignalReceived = null;

let _doc_count = 0;
let _prev_doc_count = 0;

let updateDB = false;
let updateDBStats = false;

let limitReversal = 0;
let limitLongs = 0;
let limitShorts = 0;

let orderVal = 0;

let tempLimit_A = 0;
let tempLimit_B = 0;
let tempLimit_C = 0;

let _guardUpdateCounter = 0;
let guardCounter = 0;

if (typeof localStorage === 'undefined' || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}
var nano = require('nano')('http://localhost:5984');

class MrGreenDatabaseST {
  constructor() {
    this._data = [];
    this._counter = 0;

    this.insert('exa34mple 0');
    this.insert('exa34mple 1');
  }
  all() {
    return Promise.resolve(this._data);
  }
  test(val) {
    return Promise.resolve('alchemy -' + val + ' ._o ');
    //return Promise.resolve(val.toString());
  }
  byId(id) {
    return Promise.resolve(this._data[id]);
  }
  addTodoOld(val) {
    //console.log(val);
    //var text = val;
    var todo = {
      _id: new Date().toISOString(),
      title: val,
      completed: false,
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted a todo!');
        //sync();
      }
    });
    return Promise.resolve(val.toString());
  }

  // POUCH DB INTEGRATION
  //this function replaces the previous addTodo. renamed previous to addTodoOld
  addTodo(text) {
    var todo = {
      _id: new Date().toISOString(),
      title: text,
      completed: false,
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted a todo!');
        return Promise.resolve(text.toString());
      }
    });
  }

  showTodos() {
    db.allDocs({ include_docs: true, descending: true }, function(err, doc) {
      //redrawTodosUI(doc.rows);
      console.log(doc.rows);
    });
  }

  checkboxChanged(todo, event) {
    todo.completed = event.target.checked;
    db.put(todo);
  }

  deleteButtonPressed(todo) {
    db.remove(todo);
  }

  // END POUCH DB STUFF

  /*   setGuardValue(guardValue) {
     //console.log(val);
     //var text = val;
     var todo = {
       _id: new Date().toISOString(),
       title: guardValue,
       guardValue: guardValue,
       completed: false
     };
     db_st.put(todo, function callback(err, result) {
       if (!err) {
         console.log('Successfully posted guard value!');
         //sync();
       }
     });
     
     return Promise.resolve(guardValue);
   }*/
  setGuardDirection(LSLStart, SSLStart) {
    if (LSLStart > 0) {
      stoploss_start_long = 1;
      stoploss_start_short = 0;
    }
    if (SSLStart > 0) {
      stoploss_start_short = 1;
      stoploss_start_long = 0;
    }
    return Promise.resolve(guardDirection);
  }

  updateSuperTrendInfo(
    _lsl,
    _ssl,
    _midprice,
    _lslstart,
    _sslstart,
    _stbuylabel,
    _stselllabel
  ) {
    l.info(
      `${this.constructor.name}`,
      '  prepping query ',
      _lsl,
      _ssl,
      _midprice,
      _lslstart,
      _sslstart,
      _stbuylabel,
      _stselllabel
    );

    _lsl = roundHalf(_lsl);
    _ssl = roundHalf(_ssl);
    _midprice = roundHalf(_midprice);
    _lslstart = roundHalf(_lslstart);
    _sslstart = roundHalf(_sslstart);
    //_stbuylabel = roundHalf( _stbuylabel);
    //_stselllabel = roundHalf( _stselllabel);
    _stbuylabel = 0;
    _stselllabel = 0;

    console.log(
      ' updating db  ',
      _lsl,
      _ssl,
      _midprice,
      _lslstart,
      _sslstart,
      _stbuylabel,
      _stselllabel
    );
    var todo = {
      _id: new Date().toISOString(),
      title: (stoploss_target_long, stoploss_target_short, midprice),
      stoploss_target_long: _lsl,
      stoploss_target_short: _ssl,
      stoploss_start_short: _sslstart,
      stoploss_start_long: _lslstart,
      stoploss_reversal_short: stoploss_reversal_short,
      stoploss_reversal_long: stoploss_reversal_long,
      stoploss_orderid_short: stoploss_orderid_short,
      stoploss_orderid_long: stoploss_orderid_long,
      stoplossReversal: stoplossReversal,
      supertrendbuylabel: _stbuylabel,
      supertrendselllabel: _stselllabel,
      midprice: _midprice,
      completed: false,
    };
    stoploss_target_long = roundHalf(_lsl);
    stoploss_target_short = roundHalf(_ssl);
    stoploss_start_short = roundHalf(_sslstart);
    stoploss_start_long = roundHalf(_lslstart);
    supertrendbuylabel = roundHalf(_stbuylabel);
    supertrendselllabel = roundHalf(_stselllabel);
    midprice = roundHalf(_midprice);

    prev_stoploss_target_long = current_stoploss_target_long;
    prev_stoploss_target_short = current_stoploss_target_short;
    current_stoploss_target_long = stoploss_target_long;
    current_stoploss_target_short = stoploss_target_short;

    process.env.PREV_LSL = prev_stoploss_target_long;
    process.env.PREV_SSL = prev_stoploss_target_short;
    process.env.CURRENT_LSL = current_stoploss_target_long;
    process.env.CURRENT_SSL = current_stoploss_target_short;

    localStorage.setItem('_lsl', stoploss_target_long);
    localStorage.setItem('_ssl', stoploss_target_short);
    localStorage.setItem('_midprice', _midprice);
    localStorage.setItem('_lslstart', _lslstart);
    localStorage.setItem('_sslstart', _sslstart);
    localStorage.setItem('_stbuylabel', _stbuylabel);
    localStorage.setItem('_stselllabel', _stselllabel);

    let updateDBStats = true;

    //var guardCounter = localStorage.getItem("_guardUpdateCounter");
    //guardCounter = parseFloat(guardCounter) + 1;

    //localStorage.setItem("_guardUpdateCounter", guardCounter)

    db_st.put(todo, function callback(err, result) {
      console.log('estuff');
      if (!err) {
        console.log('Successfully posted guard info - updateSuperTrendInfo');
      }
      if (err) {
        console.log('error');
      }
    });
    ///////////////////
    processGuards();
    // console.log(process.env.PREV_LSL);
    console.log('process guards');
    //////////////////////////
    return Promise.resolve('DB updated');
  }

  //loss prevention
  getShortStopLossStart() {
    var _ssl = localStorage.getItem('_ssl');
    console.log(_ssl);
  }
  shortStopLossUpdate(sslUpdate) {
    stoploss_target_short = sslUpdate;
    return Promise.resolve(sslUpdate);
  }
  longStopLossUpdate(lslUpdate) {
    stoploss_target_long = lslUpdate;
    return Promise.resolve(lslUpdate);
  }

  setLimitLongs(limitLongs) {
    return Promise.resolve(limitLongs);
  }
  setLimitShorts(limitShorts) {
    return Promise.resolve(limitShorts);
  }
  setLimitReversal(limitReversal) {
    return Promise.resolve(limitReversal);
  }
  setSignalLong(signal, time) {
    latestSignal = signal;
    latestTimeSignalReceived = time;
    updateDB = true;
    return;
    //Promise.resolve(signal, time);
  }
  setSignalShort(signal, time) {
    latestSignal = signal;
    latestTimeSignalReceived = time;
    updateDB = true;
    return;
    //Promise.resolve(signal, time);
  }
  enableStopLossReversal() {
    stoplossReversal = true;
    return Promise.resolve('stoploss reversal enabled');
  }
  disableStopLossReversal() {
    stoplossReversal = false;
    return Promise.resolve('stoploss reversal disabled');
  }
  

  instrumentDev() {
    console.log('order from instrumentDev ', order);
    return client.createOrder(8000, 1);

    var order = client.position();

    //console.log('position ', position);

    return Promise.resolve(position);
  }

  //////////////////
  showTodos() {
    db_st.allDocs(
      {
        include_docs: true,
        descending: true,
      },
      function(err, doc) {
        if (!err) {
          console.log('Successfully called to show all todo!');
          redrawTodosUI(doc.rows);
        }
      }
    );
  }
  insert(name) {
    const record = {
      id: this._counter,
      name,
    };

    this._counter += 1;
    this._data.push(record);

    return Promise.resolve(record);
  }
  checkboxChanged(todo, event) {
    todo.completed = event.target.checked;
    db.put(todo);
  }
  deleteButtonPressed(todo) {
    db.remove(todo);
  }
}

// Given an object representing a todo, this will create a list item
// to display it.
function createTodoListItem(todo) {
  var checkbox = document.createElement('input');
  checkbox.className = 'toggle';
  checkbox.type = 'checkbox';
  checkbox.addEventListener('change', checkboxChanged.bind(this, todo));

  var label = document.createElement('label');
  label.appendChild(document.createTextNode(todo.title));
  label.addEventListener('dblclick', todoDblClicked.bind(this, todo));

  var deleteLink = document.createElement('button');
  deleteLink.className = 'destroy';
  deleteLink.addEventListener('click', deleteButtonPressed.bind(this, todo));

  var divDisplay = document.createElement('div');
  divDisplay.className = 'view';
  divDisplay.appendChild(checkbox);
  divDisplay.appendChild(label);
  divDisplay.appendChild(deleteLink);

  var inputEditTodo = document.createElement('input');
  inputEditTodo.id = 'input_' + todo._id;
  inputEditTodo.className = 'edit';
  inputEditTodo.value = todo.title;
  inputEditTodo.addEventListener('keypress', todoKeyPressed.bind(this, todo));
  inputEditTodo.addEventListener('blur', todoBlurred.bind(this, todo));

  var li = document.createElement('li');
  li.id = 'li_' + todo._id;
  li.appendChild(divDisplay);
  li.appendChild(inputEditTodo);

  if (todo.completed) {
    li.className += 'complete';
    checkbox.checked = true;
  }

  return li;
}

function redrawTodosUI(todos) {
  var ul = document.getElementById('todo-list');
  ul.innerHTML = '';
  todos.forEach(function(todo) {
    ul.appendChild(createTodoListItem(todo.doc));
  });
}

function sync_st() {
  //syncDom.setAttribute('data-sync-state', 'syncing');
  var opts = {
    live: true,
  };
  db_st.replicate.to(remoteCouch_st, opts, syncError);
  db_st.replicate.from(remoteCouch_st, opts, syncError);
  console.log('st synced');
}

function syncError() {
  //syncDom.setAttribute('data-sync-state', 'error');
}

function showTodos() {
  db_st.allDocs(
    {
      include_docs: true,
      descending: true,
    },
    function(err, doc) {
      if (!err) {
        console.log('Successfully called to show all todo!');
      }
      if (err) {
        console.log('error at 224-db_st');
      }
    }
  );
}

function roundHalf(num) {
  return Math.round(num * 2) / 2;
}

showTodos();

if (remoteCouch_st !== '') {
  sync_st();
}

//for communicating wiht livenet for stoploss orders

const livenet = async () => {
  try {
    //will use later to curl for livenet communictaion
    console.log(
      ' P. LSL ',
      process.env.PREV_LSL,
      'P. SSL ',
      process.env.PREV_SSL,
      ' C. LSL ',
      process.env.CURRENT_LSL,
      ' C.SSL ',
      process.env.CURRENT_SSL
    );
    checkGuardPosition();
    return await axios.get('http://mrgreenswag.ddns.net:5984/supertrend');
  } catch (error) {
    console.error(error);
  }
};

const processGuards = async () => {
  const guards = await livenet();
  if (guards.data.message) {
    console.log(`Got ${Object.entries(guards.data.message).length} guards`);
    superTrendProcess();
  }
};

function checkGuardPosition() {
  console.log(
    'checking guad',
    prev_stoploss_target_long,
    prev_stoploss_target_short,
    current_stoploss_target_long,
    current_stoploss_target_short
  );
  //CHECK FOR CLOSING OUT STOPS

  if (prev_stoploss_target_long > 0) {
    console.log('//need to close out long guard');
    if (prev_stoploss_target_long === 0) {
      console.log('//need to close out long guard');
      //need to close out long guard
    }
  }
  if (prev_stoploss_target_short > 0) {
    console.log('//need to close out short guard');
    if (prev_stoploss_target_short === 0) {
      //need to close out short guard
      console.log('//need to close out short guard');
    }
  }

  //OPEN GUARD IF NEEDS BE
  if (prev_stoploss_target_long === 0) {
    //console.log('//need to open long guard');
    if (current_stoploss_target_long > 0) {
      //need to open long guard
      console.log('//need to open long guard');
    }
  }
  if (prev_stoploss_target_short === 0) {
    //console.log('//need to open long guard');
    if (current_stoploss_target_short > 0) {
      //need to open short guard
      console.log('//need to open short guard');

      //FOR MARKET
      //orderQty
      //stopPX

      //FOR LIMIT
      //do command w livenet
    }
  }
}

function superTrendProcess() {
  console.log('stoploss long more then 0');
  checkGuardPosition();

  console.log('stoploss long more then 0');
  if (process.env.PREV_LSL !== process.env.CURRENT_LSL) {
    checkGuardPosition(); //order one
    //update price
  }
}

function guardCheck() {
  //_guardUpdateCounter = localStorage.getItem("_guardUpdateCounter")
  //_guardUpdateCounter++;
  //localStorage.setItem("_lsl", 0)

  let shortSL = localStorage.getItem('_ssl');
  let longSL = localStorage.getItem('_lsl');

  // l.info('guardCheck db_st.service ', longSL, shortSL, guardCounter, _guardUpdateCounter);

  /*if (_guardUpdateCounter != null) {
    var t1 = localStorage.setItem("_guardUpdateCounter", _guardUpdateCounter);
    // console.log('-', t1, guardCounter, _guardUpdateCounter);
  }
  if (guardCounter < _guardUpdateCounter) {

   
  } */
  //update counter
  guardCounter = _guardUpdateCounter;
  //console.log(guardCounter)

  //grab info
  //var shortSL = localStorage.getItem("ssl");
  //var longSL = localStorage.getItem("lsl");
  // console.log(shortSL, longSL);
  //var t1 = positionCheck();
  l.info(
    'INFO:',
    guardCounter,
    ' SSL: ',
    shortSL,
    ' LSL: ',
    longSL,
    'CurrentQty: ',
    currentQty,
    'CurrentSignalUT',
    currentSignal,
    'Halfsies:',
    halfsies,
    'negHalf ',
    negativeHalfsies,
    'limitLongs',
    limitLongs,
    'limitShorts',
    limitShorts,
    'limitReversal',
    limitReversal,
    ' ----- need PNL displayed '
    //t1
  );

  //deploy guards
  if (shortSL > 0) {
    console.log('deploy shortGuard');
  }
  if (longSL > 0) {
    console.log('deploy longGuard');
  }
}

function positionCheck() {
 // currentQty = localStorage.getItem('currentQty');
//localStorage.setItem('currentQty', 0);
l.info("GETPOSITIONINFO - POSITIONCHECK");
     (async function main() {
      try {
        return await axios.get(
          'http://mrgreenswag.ddns.net/api/mrgreen/getPositionInfo/?apiKey=Z'+ key +'&apiSecret='+ secret +'&symbol=XBTUSD'
        );
      } catch (e) {
        console.error(e);
      }
    })();
  
}

function cancelCheck(req, res) {}

function statusCheck(req, res) {
  currentQty = localStorage.getItem('currentQty');
  limitReversal = localStorage.getItem('limitReversal');
  currentSignal = localStorage.getItem('currentSignal');
}

function syncDB(){
  console.log('syndb')
  nano.db.replicate('http://teretz:silver09@mrgreenswag.ddns.net:5984/alice', 'http://teretz:silver09@localhost:5984/alice',
                    { create_target:true }, function(err, body) {
      if (!err) 
        //console.log(body);
      //  console.log();
        respondToNewOrdersFromDB();
  });
  nano.db.replicate('http://teretz:silver09@mrgreenswag.ddns.net:5984/stats', 'http://teretz:silver09@localhost:5984/stats',
                    { create_target:true }, function(err, body) {
      if (!err) 
        respondToNewStats();
  });
}

function respondToNewStats(){
  console.log('respond to new stats from sync')
  nano.db.get('stats', { revs_info: true, id: 'stats' }, function(err, body) {
    if (!err) {
      console.log(' th1 ',body, ' NewStats');
    }
  });
}

//this is where I can use the previous length to determine if there's an update
function respondToNewOrdersFromDB(){
  console.log('respond to new orders from couchdb sync')
  nano.db.get('alice', { revs_info: true, id: 'buy' }, function(err, body) {
    if (!err) {
      console.log(' no error synbcing');
      _doc_count = body.doc_count;
      if (_prev_doc_count !== _doc_count){
        _prev_doc_count = _doc_count;
        console.log('doccount ', _doc_count)
      }

    }
  });
}

function newOrderCheck(req, res) {
  l.info('NewORderChecK');
  currentQty = localStorage.getItem('currentQty');
  limitReversal = localStorage.getItem('limitReversal');
  halfsies = limitReversal / 2;
  negativeHalfsies = halfsies - halfsies - halfsies;
  currentSignal = localStorage.getItem('latestSignal');

  tempLimit_A = parseInt(halfsies) + parseInt(currentQty);
  tempLimit_B = parseInt(currentQty) + parseInt(negativeHalfsies);

  if (currentQty <= halfsies) {
  }
  if (currentQty > negativeHalfsies) {
    //ll.info('halfsies-nHalfsies = ', halfsies - negativeHalfsies);
  }

  if (currentSignal === 'Long') {
    side = 'Buy';
    if (currentQty + qtyToFill < 0) {
      if (parseInt(currentQty) == parseInt(negativeHalfsies)) {
        l.info('adapt long for full amount ', tempLimit_A);
        doFullLongAdapt();
      } else {
        l.info('adapt long for large amount ', tempLimit_A);
        doLargeLongAdapt();
      }
    } else if (currentQty + qtyToFill == 0) {
      l.info('adapt long for half amount ', tempLimit_A);
      doZeroLongAdapt();
    } else if (currentQty + qtyToFill > 0) {
      console.log(currentQty, halfsies);
      if (parseInt(currentQty) == parseInt(halfsies)) {
        l.info('adapt long for no amount ');
        doNothing();
      } else {
        l.info('adapt long for small amount ', tempLimit_A);
        doSmallLongAdapt();
      }
    }
  }
  if (currentSignal === 'Short') {
    side = 'Sell';
    if (currentQty < 0) {
      if (parseInt(currentQty) === parseInt(negativeHalfsies)) {
        l.info('adapt long for no amount ', tempLimit_B);
        doNothing();
      } else {
        l.info('adapt long for small amount ', tempLimit_B);
        doSmallShortAdapt();
      }
    } else if (currentQty == 0) {
      l.info('adapt short for half amount ', tempLimit_B);
      doZeroShortAdapt();
    } else if (currentQty > 0) {
      console.log(currentQty, halfsies);
      if (parseInt(currentQty) === parseInt(halfsies)) {
        l.info('adapt long for full limit amount ', tempLimit_B);
        doFullShortAdapt();
      } else {
        l.info('adapt long for larger amount ', tempLimit_B);
        doLargeShortAdapt();
      }
    }
  }

  if (orderPlaced === true) {
    orderCounter = orderCounter + 1;
  }

  if (orderCounter === 10) {
    cancelOrders();
    orderPlaced = false;
    orderCounter = 0;
  }

}
function doFullLongAdapt() {
  console.log('doFullLongAdapt');
  if (orderPlaced === false) {
    if (currentSignal === 'Long') {
      side = 'Buy';
      qtyToFill = limitReversal;
      orderPlaced = true;
    }
    if (currentSignal === 'Short') {
      side = 'Sell';
      qtyToFill = limitReversal;
      orderPlaced = true;
    }

    var url =
      'http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      limitReversal; //, limitReversal.toString());
    l.info(side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('//mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doLargeLongAdapt() {}

function doZeroLongAdapt() {
  console.log('doZeroLongAdapt');
  if (orderPlaced === false) {
    orderPlaced = true;
    if (currentSignal === 'Long') {
      side = 'Buy';
      qtyToFill = limitLongs;
      orderPlaced = true;
    }
    if (currentSignal === 'Short') {
      side = 'Sell';
      qtyToFill = limitShorts;
      orderPlaced = true;
    }

    var url =
      'http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      tempLimit_A; //, limitReversal.toString());
    l.info(side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doSmallLongAdapt() {}

function doNothing() {
  l.info('doing nothing');
}

function doFullShortAdapt() {
  console.log('doFullShortAdapt');
  if (orderPlaced === false) {
    orderPlaced = true;
    if (currentSignal === 'Long') {
      side = 'Buy';
      qtyToFill = limitReversal;
      orderPlaced = true;
    }
    if (currentSignal === 'Short') {
      side = 'Sell';
      qtyToFill = limitReversal;
      orderPlaced = true;
    }

    var url =
      'http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      limitReversal; //, limitReversal.toString());
    l.info(side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doLargeShortAdapt() {}

function doZeroShortAdapt() {
  console.log('doZeroShortAdapt');
  if (orderPlaced === false) {
    orderPlaced = true;
    if (currentSignal === 'Long') {
      side = 'Buy';
      qtyToFill = limitLongs;
    }
    if (currentSignal === 'Short') {
      side = 'Sell';
      qtyToFill = limitShorts;
    }

    var url =
      'http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      tempLimit_B; //, limitReversal.toString());
    l.info(side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doSmallShortAdapt() {}

function cancelOrders() {
  var url =
    'http://mrgreenswag.ddns.net/api/mrgreen/cancelOrders/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA'; //, limitReversal.toString());

  (async function main() {
    try {
      return await axios.post(url);
      //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
    } catch (e) {
      console.error(e);
    }
  })();
}

function updateDBCheck(req, res) {
  checkRevs();
  currentQty = localStorage.getItem('currentQty');
  limitReversal = localStorage.getItem('limitReversal');
  currentSignal = localStorage.getItem('latestSignal');
  console.log('update db check');
  //this is for if a buy or sell signal comes thru, this will execute adding it to DB
  if (updateDB === true) {
    var _buyRev = localStorage.getItem("_buy_rev");
    var _sellRev = localStorage.getItem("_sell_rev");
    console.log('updateDB = true', _buyRev, _sellRev);
    updateDB = false;
    // specify the database we are going to use
    var alice = nano.use('alice');
    // and insert a document in it
    if (currentSignal === 'Long') {
      alice.insert({ long: true, _rev:_buyRev }, 'buy', function(err, body) {
        if (err) {
          console.log('[alice.insert] ', err.message);
          updateDB = true;
          return;
        }
        console.log(
          'td has sent a buy order and this has been updated in the db.'
        );
        console.log(body);
      });
    }
    if (currentSignal === 'Short') {
      alice.insert({ short: true, _rev:_sellRev }, 'sell', function(err, body) {
        if (err) {
          console.log('[alice.insert] ', err.message);
          updateDB = true;
          return;
        }
        console.log(
          'td has sent a sell order and this has been updated in the db.'
        );
        console.log(body);
      });
    }
    updateDBStats = true;
  }
}

function updateCurrentStats(req, res) {
  checkRevs();
 
  limitReversal = localStorage.getItem('limitReversal');
  currentSignal = localStorage.getItem('latestSignal');
  console.log('update db stats');
  //this is for if a buy or sell signal comes thru, this will execute adding it to DB
  if (updateDBStats === true) {
    var _statsRev = localStorage.getItem("_stats_rev");
    console.log('updateDBStats = true', _statsRev);
    updateDBStats = false;
    // specify the database we are going to use
    var stats = nano.use('stats');
    // and insert a document in it
    

  var  _lsl = localStorage.getItem('_lsl');
  var  _ssl = localStorage.getItem('_ssl');
  var  _midprice = localStorage.getItem('_midprice');
  var  _lslstart = localStorage.getItem('_lslstart');
  var  _sslstart = localStorage.getItem('_sslstart');
  var  _stbuylabel = localStorage.getItem('_stbuylabel');
  var  _stselllabel = localStorage.getItem('_stselllabel');
  var _limitReversal = localStorage.getItem('limitReversal');
  var _currentSignal = localStorage.getItem('latestSignal');

      stats.insert(  { _id: 'stats', limitReversal:_limitReversal, currentSignal:_currentSignal, currentQty: currentQty, lsl: _lsl, ssl:_ssl, midprice:_midprice, lslstart:_lslstart, sslstart:_sslstart, _rev:_statsRev }, 'stats',  function(err, body) {
        if (err) {
          console.log('[stats.insert] ', err.message);
          updateDBStats = true;
          return;
        }
        console.log(body);
      });
    
  }
}

function checkRevs(){
  var alice = nano.use('alice');
  alice.get('buy', { revs_info: true }, function(err, body) {
    if (!err){
      localStorage.setItem('_buy_rev', body._rev);
      //console.log( 'body', body);
    }
  });
  alice.get('sell', { revs_info: true }, function(err, body) {
    if (!err){
      localStorage.setItem('_sell_rev', body._rev);
      //console.log( 'body', body);
    }
  });

  var alice_stats = nano.use('stats');
  alice_stats.get('stats', { revs_info: true }, function(err, body) {
    if (!err){
      localStorage.setItem('_stats_rev', body._rev);
      localStorage.setItem('currentQty', body.currentQty);
      //localStorage.setItem('currentSignal', body.currentSignal);
      localStorage.setItem('limitReversal', body.limitReversal);

      var __lsl = localStorage.getItem('_lsl');

      if (body.lsl > 0) {
        localStorage.setItem('_lsl', body.lsl);
        l.info('BODY.LSL ', body.lsl);
      }
      if(body.lslstart > 0){
        localStorage.setItem('_lslstart', body.lslstart);
      }
      if(body.sslstart > 0 ){
        localStorage.setItem('_sslstart', body.sslstart);
      }
      if(body.ssl){
        localStorage.setItem('_ssl', body.ssl);
      }
    }
  });

  var alice_orders = nano.use('orders');
  alice_orders.get('latestOrder', { revs_info: true }, function(err, body) {
    if (!err){
     localStorage.setItem('_orders_rev', body._rev);
      

      


    }
  });
}

function getLocalStats(){
  var  _lsl = localStorage.getItem('_lsl');
  var  _ssl = localStorage.getItem('_ssl');
  var  _midprice = localStorage.getItem('_midprice');
  var  _lslstart = localStorage.getItem('_lslstart');
  var  _sslstart = localStorage.getItem('_sslstart');
  var  _stbuylabel = localStorage.getItem('_stbuylabel');
  var  _stselllabel = localStorage.getItem('_stselllabel');
  var _limitReversal = localStorage.getItem('limitReversal');
  var _currentSignal = localStorage.getItem('latestSignal');
}
function placeAdaptLimit(){

  //var _orders_rev = localStorage.getItem("_orders_rev");
  var _orders_rev = '1';
 // var adapt = new adaptiveLimit(key, secret, 30, 'Buy');
 // return adapt;
   //above in development still; for now, a simple fix taht allows us to continue work on the misfiring of orders 
   var orders = nano.use('orders');
  orderPlaced=false;

  if(currentSignal === 'Long'){
  doFullLongAdapt();
  orders.insert(  { _id: 'latestOrder',  _rev: _orders_rev, side: side }, 'orders',  function(err, body) {
    if (err) {
      console.log('[orders.insert] ', err.message);
      updateDBStats = true;
      return;
    }
    console.log(body);
  });
  }
  if(currentSignal === 'Short'){
    doFullShortAdapt();
    orders.insert(  { _id: 'latestOrder',  _rev:_orders_rev }, 'orders',  function(err, body) {
      if (err) {
        console.log('[stats.insert] ', err.message);
        updateDBStats = true;
        return;
      }
      console.log(body);
    });
    }
}



function logSignals(){
  signalCounter = signalCounter +1 ;

  l.info('counter since last signal update', signalCounter, currentSignal, prevSignal);
  if(currentSignal != prevSignal){
    if(currentSignal!=null){
      if(prevSignal!=null){
    signalCounter = 0;
    l.info('update prevSignal');
    prevSignal = currentSignal;
    placeAdaptLimit();
    }
  }
  }
}


setInterval(updateDBCheck, 10000);
setInterval(updateCurrentStats, 10000);
setInterval(statusCheck, 10000);
setInterval(logSignals, 11000);

setInterval(positionCheck, 30000);
setInterval(guardCheck, 10000);
setInterval(cancelCheck, 10000);
setInterval(newOrderCheck, 36000);
//setInterval(doFullLongAdapt, 8000);

setInterval(syncDB, 15000);
getLocalStats();

export default new MrGreenDatabaseST();
