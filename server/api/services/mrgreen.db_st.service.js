'use strict';

import {
  signedCookie
} from 'cookie-parser';
import l from '../../common/logger';

require('util').inspect.defaultOptions.depth = null;

let halfsies;
let negativeHalfsies;
let signalCounter = 0;
let side;
let env = require('dotenv')

let unrealisedPnl = 0;
let unrealisedPnlPcnt = 0;
let unrealisedRoePcnt = 0;
let avgCostPrice = 0;
let avgEntryPrice = 0;
let breakEvenPrice = 0;

let replaceOrder = false;
let updatePrimaryOrderID = false;
let adjustPrimaryOrder = false;
let primaryOrderID = 'weeerd';

let prevSignal = 'none';

const takeProfit = require('../../common/takeProfit')

var express = require('express'),
  app = express(),
  PouchDB = require('pouchdb');

app.use(express.json());
const axios = require('axios');
let localStorage = null;
const bodyParser = require('body-parser');
const path = require('path');
var request = require('request');
let globals = require('../../common/globals.js');
//const adaptiveLimit = require('../../common/adaptiveLimit');
//const adaptiveLimit = require('../../common/adapt');

const ws = require('../../common/ws');

const PrimaryOrder = require('../../common/primaryOrder.js');

PouchDB.plugin(require('pouchdb-find'));
app.use('/api/mrgreen', require('express-pouchdb')(PouchDB));

//let db = new PouchDB('supertrend');
var db_st = new PouchDB('supertrend');


//const Client = require('/home/ubuntu/mr-greenswag/server/node-bitmex/Client');
// const Client = require(process.env.NODEBITMEX_CLIENT);
const Client = require('../../node-bitmex/Client.js');

//const clientPath = path.join(process.cwd() , '/server/node-bitmex/Client');
//const Client = require(clientPath);
const key = process.env.API_KEY;
const secret = process.env.API_SECRET;
const client = new Client(key, secret);

let newPrimaryOrderPlaced = true;

let amendCounter = 0;
let testingOrdersize = 30;
let testing = true;
let currentQty = 0;
let qtyToFill = 0;
let currentSignal = 'Buy';
let orderPlaced = true;
let orderCounter = 0;
//for what value/pip to assign the loss at
let stoploss_target_short = 0;
let stoploss_target_long = 0;
let ssl = stoploss_target_short;
let lsl = stoploss_target_long;
let midprice = 0;
let askPrice = 0;
let bidPrice = 0;
let stoploss_start_short = 0;
let stoploss_start_long = 0;

let stoploss_held_short = 0;
let stoploss_held_long = 0;

let stoploss_reversal_short = 0;
let stoploss_reversal_long = 0;

let stoploss_orderid_short = '';
let stoploss_orderid_long = '';

let stoplossReversal = false;

let stoploss_direction = 'none';

let supertrendbuylabel = 0;
let supertrendselllabel = 0;

let remoteCouch_st = 'http://localhost:5984/supertrend';

let prev_stoploss_target_long = 0;
let prev_stoploss_target_short = 0;
let current_stoploss_target_long = 0;
let current_stoploss_target_short = 0;

let latestSignal = 'Sell';
let latestTimeSignalReceived = null;

let _doc_count = 0;
let _prev_doc_count = 0;

let updateDB = false;
let updateDBStats = false;

let limitReversal = 0;
let limitLongs = 0;
let limitShorts = 0;

let orderVal = 0;

let tempLimit_A = 0;
let tempLimit_B = 0;
let tempLimit_C = 0;

let _guardUpdateCounter = 0;
let guardCounter = 0;


let currentOrderComplete = false
let currentOrderCancel = false
let currentOrderFilled = false
let currentOrderPrice = 0;
let currentOrdStatus = '';
let currentOrderQty = 0;



if (typeof localStorage === 'undefined' || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}
localStorage.setItem('currentQty', 0)
var nano = require('nano')('http://localhost:5984');

class MrGreenDatabaseST {
  constructor() {
    this._data = [];
    this._counter = 0;

    this.insert('exa34mple 0');
    this.insert('exa34mple 1');
  }
  all() {
    return Promise.resolve(this._data);
  }
  test(val) {
    return Promise.resolve('alchemy -' + val + ' ._o ');
    //return Promise.resolve(val.toString());
  }
  byId(id) {
    return Promise.resolve(this._data[id]);
  }
  addTodoOld(val) {
    //console.log(val);
    //var text = val;
    var todo = {
      _id: new Date().toISOString(),
      title: val,
      completed: false,
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted a todo!');
        //sync();
      }
    });
    return Promise.resolve(val.toString());
  }

  // POUCH DB INTEGRATION
  //this function replaces the previous addTodo. renamed previous to addTodoOld
  addTodo(text) {
    var todo = {
      _id: new Date().toISOString(),
      title: text,
      completed: false,
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted a todo!');
        return Promise.resolve(text.toString());
      }
    });
  }

  showTodos() {
    db.allDocs({
      include_docs: true,
      descending: true
    }, function (err, doc) {
      //redrawTodosUI(doc.rows);
      console.log(doc.rows);
    });
  }

  checkboxChanged(todo, event) {
    todo.completed = event.target.checked;
    db.put(todo);
  }

  deleteButtonPressed(todo) {
    db.remove(todo);
  }

  // END POUCH DB STUFF

  /*   setGuardValue(guardValue) {
     //console.log(val);
     //var text = val;
     var todo = {
       _id: new Date().toISOString(),
       title: guardValue,
       guardValue: guardValue,
       completed: false
     };
     db_st.put(todo, function callback(err, result) {
       if (!err) {
         console.log('Successfully posted guard value!');
         //sync();
       }
     });
     
     return Promise.resolve(guardValue);
   }*/
  setGuardDirection(LSLStart, SSLStart) {
    if (LSLStart > 0) {
      stoploss_start_long = 1;
      stoploss_start_short = 0;
    }
    if (SSLStart > 0) {
      stoploss_start_short = 1;
      stoploss_start_long = 0;
    }
    return Promise.resolve(guardDirection);
  }

  updateSuperTrendInfo(
    _lsl,
    _ssl,
    _midprice,
    _lslstart,
    _sslstart,
    _stbuylabel,
    _stselllabel
  ) {
    l.info(
      `${this.constructor.name}`,
      '  prepping query ',
      _lsl,
      _ssl,
      _midprice,
      _lslstart,
      _sslstart,
      _stbuylabel,
      _stselllabel
    );

    _lsl = roundHalf(_lsl);
    _ssl = roundHalf(_ssl);
    _midprice = roundHalf(_midprice);
    _lslstart = roundHalf(_lslstart);
    _sslstart = roundHalf(_sslstart);
    //_stbuylabel = roundHalf( _stbuylabel);
    //_stselllabel = roundHalf( _stselllabel);
    _stbuylabel = 0;
    _stselllabel = 0;

    console.log(
      ' updating db  ',
      _lsl,
      _ssl,
      _midprice,
      _lslstart,
      _sslstart,
      _stbuylabel,
      _stselllabel
    );
    var todo = {
      _id: new Date().toISOString(),
      title: (stoploss_target_long, stoploss_target_short, midprice),
      stoploss_target_long: _lsl,
      stoploss_target_short: _ssl,
      stoploss_start_short: _sslstart,
      stoploss_start_long: _lslstart,
      stoploss_reversal_short: stoploss_reversal_short,
      stoploss_reversal_long: stoploss_reversal_long,
      stoploss_orderid_short: stoploss_orderid_short,
      stoploss_orderid_long: stoploss_orderid_long,
      stoplossReversal: stoplossReversal,
      supertrendbuylabel: _stbuylabel,
      supertrendselllabel: _stselllabel,
      midprice: _midprice,
      completed: false,
    };
    stoploss_target_long = roundHalf(_lsl);
    stoploss_target_short = roundHalf(_ssl);
    stoploss_start_short = roundHalf(_sslstart);
    stoploss_start_long = roundHalf(_lslstart);
    supertrendbuylabel = roundHalf(_stbuylabel);
    supertrendselllabel = roundHalf(_stselllabel);
    midprice = roundHalf(_midprice);

    prev_stoploss_target_long = current_stoploss_target_long;
    prev_stoploss_target_short = current_stoploss_target_short;
    current_stoploss_target_long = stoploss_target_long;
    current_stoploss_target_short = stoploss_target_short;

    process.env.PREV_LSL = prev_stoploss_target_long;
    process.env.PREV_SSL = prev_stoploss_target_short;
    process.env.CURRENT_LSL = current_stoploss_target_long;
    process.env.CURRENT_SSL = current_stoploss_target_short;

    localStorage.setItem('_lsl', stoploss_target_long);
    localStorage.setItem('_ssl', stoploss_target_short);
    localStorage.setItem('_midprice', _midprice);
    localStorage.setItem('_lslstart', _lslstart);
    localStorage.setItem('_sslstart', _sslstart);
    localStorage.setItem('_stbuylabel', _stbuylabel);
    localStorage.setItem('_stselllabel', _stselllabel);

    let updateDBStats = true;

    //var guardCounter = localStorage.getItem("_guardUpdateCounter");
    //guardCounter = parseFloat(guardCounter) + 1;

    //localStorage.setItem("_guardUpdateCounter", guardCounter)

    db_st.put(todo, function callback(err, result) {
      console.log('estuff');
      if (!err) {
        console.log('Successfully posted guard info - updateSuperTrendInfo');
      }
      if (err) {
        console.log('error');
      }
    });
    ///////////////////
    processGuards();
    // console.log(process.env.PREV_LSL);
    console.log('process guards');
    //////////////////////////
    return Promise.resolve('DB updated');
  }

  //loss prevention
  getShortStopLossStart() {
    var _ssl = localStorage.getItem('_ssl');
    console.log(_ssl);
  }
  shortStopLossUpdate(sslUpdate) {
    stoploss_target_short = sslUpdate;
    return Promise.resolve(sslUpdate);
  }
  longStopLossUpdate(lslUpdate) {
    stoploss_target_long = lslUpdate;
    return Promise.resolve(lslUpdate);
  }

  setLimitLongs(limitLongs) {
    return Promise.resolve(limitLongs);
  }
  setLimitShorts(limitShorts) {
    return Promise.resolve(limitShorts);
  }
  setLimitReversal(limitReversal) {
    return Promise.resolve(limitReversal);
  }
  setSignalLong(signal, time) {
    latestSignal = signal;
    latestTimeSignalReceived = time;
    updateDB = true;
    return;
    //Promise.resolve(signal, time);
  }
  setSignalShort(signal, time) {
    latestSignal = signal;
    latestTimeSignalReceived = time;
    updateDB = true;
    return;
    //Promise.resolve(signal, time);
  }
  placeWolforder() {
    latestSignal = signal;
    latestTimeSignalReceived = time;
    //updateDB = true;
    return;
    //Promise.resolve(signal, time);
  }
  enableStopLossReversal() {
    stoplossReversal = true;
    return Promise.resolve('stoploss reversal enabled');
  }
  disableStopLossReversal() {
    stoplossReversal = false;
    return Promise.resolve('stoploss reversal disabled');
  }

  instrumentDev() {
    console.log('order from instrumentDev ', order);
    return client.createOrder(8000, 1);

    var order = client.position();

    console.log('position ', position);

    return Promise.resolve(position);
  }

  //////////////////
  showTodos() {
    db_st.allDocs({
        include_docs: true,
        descending: true,
      },
      function (err, doc) {
        if (!err) {
          console.log('Successfully called to show all todo!');
          redrawTodosUI(doc.rows);
        }
      }
    );
  }
  insert(name) {
    const record = {
      id: this._counter,
      name,
    };

    this._counter += 1;
    this._data.push(record);

    return Promise.resolve(record);
  }
  checkboxChanged(todo, event) {
    todo.completed = event.target.checked;
    db.put(todo);
  }
  deleteButtonPressed(todo) {
    db.remove(todo);
  }
}

// Given an object representing a todo, this will create a list item
// to display it.
function createTodoListItem(todo) {
  var checkbox = document.createElement('input');
  checkbox.className = 'toggle';
  checkbox.type = 'checkbox';
  checkbox.addEventListener('change', checkboxChanged.bind(this, todo));

  var label = document.createElement('label');
  label.appendChild(document.createTextNode(todo.title));
  label.addEventListener('dblclick', todoDblClicked.bind(this, todo));

  var deleteLink = document.createElement('button');
  deleteLink.className = 'destroy';
  deleteLink.addEventListener('click', deleteButtonPressed.bind(this, todo));

  var divDisplay = document.createElement('div');
  divDisplay.className = 'view';
  divDisplay.appendChild(checkbox);
  divDisplay.appendChild(label);
  divDisplay.appendChild(deleteLink);

  var inputEditTodo = document.createElement('input');
  inputEditTodo.id = 'input_' + todo._id;
  inputEditTodo.className = 'edit';
  inputEditTodo.value = todo.title;
  inputEditTodo.addEventListener('keypress', todoKeyPressed.bind(this, todo));
  inputEditTodo.addEventListener('blur', todoBlurred.bind(this, todo));

  var li = document.createElement('li');
  li.id = 'li_' + todo._id;
  li.appendChild(divDisplay);
  li.appendChild(inputEditTodo);

  if (todo.completed) {
    li.className += 'complete';
    checkbox.checked = true;
  }

  return li;
}

function redrawTodosUI(todos) {
  var ul = document.getElementById('todo-list');
  ul.innerHTML = '';
  todos.forEach(function (todo) {
    ul.appendChild(createTodoListItem(todo.doc));
  });
}

function sync_st() {
  //syncDom.setAttribute('data-sync-state', 'syncing');
  var opts = {
    live: true,
  };
  db_st.replicate.to(remoteCouch_st, opts, syncError);
  db_st.replicate.from(remoteCouch_st, opts, syncError);
  console.log('st synced');
}

function syncError() {
  //syncDom.setAttribute('data-sync-state', 'error');
}

function showTodos() {
  db_st.allDocs({
      include_docs: true,
      descending: true,
    },
    function (err, doc) {
      if (!err) {
        console.log('Successfully called to show all todo!');
      }
      if (err) {
        console.log('error at 224-db_st');
      }
    }
  );
}

function roundHalf(num) {
  return Math.round(num * 2) / 2;
}

showTodos();

if (remoteCouch_st !== '') {
  sync_st();
}

//for communicating wiht livenet for stoploss orders

const livenet = async () => {
  try {
    //will use later to curl for livenet communictaion
    console.log(
      ' P. LSL ',
      process.env.PREV_LSL,
      'P. SSL ',
      process.env.PREV_SSL,
      ' C. LSL ',
      process.env.CURRENT_LSL,
      ' C.SSL ',
      process.env.CURRENT_SSL
    );
    if (process.env.CHECK_GUARD_POSITION === true) {
      checkGuardPosition();
    }
    return await axios.get('http://' + process.cwd() + ':5984/supertrend');
  } catch (error) {
    console.error(error);
  }
};

const processGuards = async () => {
  const guards = await livenet();
  if (guards.data.message) {
    console.log(`Got ${Object.entries(guards.data.message).length} guards`);
    superTrendProcess();
  }
};

function checkGuardPosition() {
  console.log(
    'checking guad',
    prev_stoploss_target_long,
    prev_stoploss_target_short,
    current_stoploss_target_long,
    current_stoploss_target_short
  );
  //CHECK FOR CLOSING OUT STOPS

  if (prev_stoploss_target_long > 0) {
    console.log('//need to close out long guard');
    if (prev_stoploss_target_long === 0) {
      console.log('//need to close out long guard');
      //need to close out long guard
    }
  }
  if (prev_stoploss_target_short > 0) {
    console.log('//need to close out short guard');
    if (prev_stoploss_target_short === 0) {
      //need to close out short guard
      console.log('//need to close out short guard');
    }
  }

  //OPEN GUARD IF NEEDS BE
  if (prev_stoploss_target_long === 0) {
    //console.log('//need to open long guard');
    if (current_stoploss_target_long > 0) {
      //need to open long guard
      console.log('//need to open long guard');
    }
  }
  if (prev_stoploss_target_short === 0) {
    //console.log('//need to open long guard');
    if (current_stoploss_target_short > 0) {
      //need to open short guard
      console.log('//need to open short guard');

      //FOR MARKET
      //orderQty
      //stopPX

      //FOR LIMIT
      //do command w livenet
    }
  }
}

function superTrendProcess() {
  console.log('stoploss long more then 0');
  if (process.env.CHECK_GUARD_POSITION == true) {
    checkGuardPosition();
  }

  console.log('stoploss long more then 0');
  if (process.env.PREV_LSL !== process.env.CURRENT_LSL) {
    if (process.env.CHECK_GUARD_POSITION === true) {
      checkGuardPosition();
    } //order one
    //update price
  }
}

function guardCheck() {
  //_guardUpdateCounter = localStorage.getItem("_guardUpdateCounter")
  //_guardUpdateCounter++;
  //localStorage.setItem("_lsl", 0)

  let shortSL = localStorage.getItem('_ssl');
  let longSL = localStorage.getItem('_lsl');

  // l.info('guardCheck db_st.service ', longSL, shortSL, guardCounter, _guardUpdateCounter);

  /*if (_guardUpdateCounter != null) {
    var t1 = localStorage.setItem("_guardUpdateCounter", _guardUpdateCounter);
    // console.log('-', t1, guardCounter, _guardUpdateCounter);
  }
  if (guardCounter < _guardUpdateCounter) {

   
  } */
  //update counter
  guardCounter = _guardUpdateCounter;
  //console.log(guardCounter)

  //grab info
  //var shortSL = localStorage.getItem("ssl");
  //var longSL = localStorage.getItem("lsl");
  // console.log(shortSL, longSL);
  //var t1 = positionCheck();
  l.info(`${this.constructor.name}`,
    'INFO: -PNL: ',
    ' uPnl '+ unrealisedPnl,
    ' uPnl% '+ unrealisedPnlPcnt,
    'uRoe% '+ unrealisedRoePcnt,
    avgCostPrice,
    avgEntryPrice,
    breakEvenPrice,
    ' - / ',
    guardCounter,
    ' SSL: ',
    shortSL,
    ' LSL: ',
    longSL,
    'CurrentQty: ',
    currentQty,
    'AskPrice:',
    localStorage.getItem('askPrice'),
    'BidPrice:',
    localStorage.getItem('bidPrice'),
    'CurrentSignalUT',
    currentSignal,
    'LatestSignal:',
    latestSignal,
    'Halfsies:',
    halfsies,
    'negHalf ',
    negativeHalfsies,
    'limitLongs',
    limitLongs,
    'limitShorts',
    limitShorts,
    'limitReversal',
    limitReversal,
    ' ----- need PNL displayed '
    //t1
  );

  //deploy guards
  if (shortSL > 0) {
    console.log('deploy shortGuard');
  }
  if (longSL > 0) {
    console.log('deploy longGuard');
  }
}

function positionCheck() {
  if (process.env.GET_POSITION === false) {
    // currentQty = localStorage.getItem('currentQty');
    //localStorage.setItem('currentQty', 0);
    (async function main() {
      try {
        return await axios.get(
          //'http://localhost:3000/api/mrgreen/getPositionInfo/?apiKey=' +
          key +
          '&apiSecret=' +
          secret +
          '&symbol=XBTUSD'
        );
      } catch (e) {
        console.error(e);
      }
      l.info('SKDJSK:JDK:S');
    })();

  }
}

function cancelCheck(req, res) {}

function statusCheck(req, res) {
  currentQty = localStorage.getItem('currentQty');
  limitReversal = localStorage.getItem('limitReversal');
  currentSignal = localStorage.getItem('latestSignal');
}

function syncDB() {
  //console.log('syndb');
  nano.db.replicate(
    'http://teretz:silver09@mrgreenswag.ddns.net:5984/alice',
    'http://teretz:silver09@localhost:5984/alice', {
      create_target: true
    },
    function (err, body) {
      if (!err)
        //console.log(body);
        //  console.log();
        respondToNewOrdersFromDB();
    }
  );
  nano.db.replicate(
    'http://teretz:silver09@mrgreenswag.ddns.net:5984/stats',
    'http://teretz:silver09@localhost:5984/stats', {
      create_target: true
    },
    function (err, body) {
      if (!err) respondToNewStats();
    }
  );
}

function respondToNewStats() {
  console.log('respond to new stats from sync');
  nano.db.get('stats', {
    revs_info: true,
    id: 'stats'
  }, function (err, body) {
    if (!err) {
      console.log(' th1 ', body, ' NewStats');
    }
  });
}

//this is where I can use the previous length to determine if there's an update
function respondToNewOrdersFromDB() {
  console.log('respond to new orders from couchdb sync');
  nano.db.get('alice', {
    revs_info: true,
    id: 'buy'
  }, function (err, body) {
    if (!err) {
      console.log(' no error synbcing');
      _doc_count = body.doc_count;
      if (_prev_doc_count !== _doc_count) {
        _prev_doc_count = _doc_count;
        console.log('doccount ', _doc_count);
      }
    }
  });
}

function newOrderCheck(req, res) {
  // l.info('NewORderChecK');
  currentQty = localStorage.getItem('currentQty');
  limitReversal = localStorage.getItem('limitReversal');
  halfsies = limitReversal / 2;
  negativeHalfsies = halfsies - halfsies - halfsies;
  currentSignal = localStorage.getItem('latestSignal');

  tempLimit_A = parseInt(halfsies) + parseInt(currentQty);
  tempLimit_B = parseInt(currentQty) + parseInt(negativeHalfsies);

  if (currentQty <= halfsies) {}
  if (currentQty > negativeHalfsies) {
    //ll.info('halfsies-nHalfsies = ', halfsies - negativeHalfsies);
  }

  if (currentSignal === 'Buy') {
    side = 'Buy';
    if (currentQty + qtyToFill < 0) {
      if (parseInt(currentQty) == parseInt(negativeHalfsies)) {
        l.info(`${this.constructor.name}`, 'adapt long for full amount ', tempLimit_A);
        doFullLongAdapt();
      } else {
        l.info(`${this.constructor.name}`, 'adapt long for large amount ', tempLimit_A);
        doLargeLongAdapt();
      }
    } else if (currentQty + qtyToFill == 0) {
      l.info('adapt long for half amount ', tempLimit_A);
      doZeroLongAdapt();
    } else if (currentQty + qtyToFill > 0) {
      console.log(currentQty, halfsies);
      if (parseInt(currentQty) == parseInt(halfsies)) {
        l.info('adapt long for no amount ');
        doNothing();
      } else {
        l.info('adapt long for small amount ', tempLimit_A);
        doSmallLongAdapt();
      }
    }
  }
  if (currentSignal === 'Sell') {
    side = 'Sell';
    if (currentQty < 0) {
      if (parseInt(currentQty) === parseInt(negativeHalfsies)) {
        l.info('adapt long for no amount ', tempLimit_B);
        doNothing();
      } else {
        l.info('adapt long for small amount ', tempLimit_B);
        doSmallShortAdapt();
      }
    } else if (currentQty == 0) {
      l.info('adapt short for half amount ', tempLimit_B);
      doZeroShortAdapt();
    } else if (currentQty > 0) {
      console.log(currentQty, halfsies);
      if (parseInt(currentQty) === parseInt(halfsies)) {
        l.info('adapt long for full limit amount ', tempLimit_B);
        doFullShortAdapt();
      } else {
        l.info('adapt long for larger amount ', tempLimit_B);
        doLargeShortAdapt();
      }
    }
  }

  if (orderPlaced === true) {
    orderCounter = orderCounter + 1;
  }

  if (orderCounter === 10) {


    orderCounter = 0;
  }
}

function doFullLongAdapt() {
  console.log('doFullLongAdapt');
  if (orderPlaced === false) {
    if (currentSignal === 'Buy') {
      side = 'Buy';
      qtyToFill = limitReversal;
      orderPlaced = true;
    }
    if (currentSignal === 'Sell') {
      side = 'Sell';
      qtyToFill = limitReversal;
      orderPlaced = true;
    }
    var t1 = localStorage.getItem('currentOrderQty');
    var url =
      'http://' + process.env.ADAPTIVE_SERVER + '/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      t1; //, limitReversal.toString());
    l.info(`${this.constructor.name}`, side, limitReversal, url);
    (async function main() {
      if (orderPlaced === false) {
        try {
          return await axios.post(url);
          //return await axios.get('//mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
        } catch (e) {
          console.error(e);
        }
      }
    })();
  }
}

function doLargeLongAdapt() {}

function doZeroLongAdapt() {
  console.log('doZeroLongAdapt');
  if (orderPlaced === false) {
    orderPlaced = true;
    if (currentSignal === 'Buy') {
      side = 'Buy';
      qtyToFill = limitLongs;
      orderPlaced = true;
    }
    if (currentSignal === 'Sell') {
      side = 'Sell';
      qtyToFill = limitShorts;
      orderPlaced = true;
    }

    var url =
      'http://' + process.cwd() + '/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      tempLimit_A; //, limitReversal.toString());
    l.info(`${this.constructor.name}`, side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doSmallLongAdapt() {}

function doNothing() {
  l.info('doing nothing');
}

function doFullShortAdapt() {
  var t1 = localStorage.getItem('currentOrderQty');
  console.log('doFullShortAdapt', );
  if (orderPlaced === false) {
    orderPlaced = true;
    if (currentSignal === 'Buy') {
      side = 'Buy';

      qtyToFill = t1;
      orderPlaced = true;
    }
    if (currentSignal === 'Sell') {
      side = 'Sell';
      qtyToFill = t1;
      orderPlaced = true;
    }
    var url =
      'http://' + process.env.ADAPTIVE_SERVER + '/api/mrgreen/adaptReversal/?apiKey=' + process.env.API_KEY + '&apiSecret=' + process.env.API_SECRET + '&side=' +
      side +
      '&amount=' +
      t1; //, limitReversal.toString());
    l.info(' doing full short adapt', side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doLargeShortAdapt() {}

function doZeroShortAdapt() {
  console.log('doZeroShortAdapt');
  if (orderPlaced === false) {
    orderPlaced = true;
    if (currentSignal === 'Buy') {
      side = 'Buy';
      qtyToFill = limitLongs;
    }
    if (currentSignal === 'Sell') {
      side = 'Sell';
      qtyToFill = limitShorts;
    }

    var url =
      'http://' + process.cwd() + '/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=' +
      side +
      '&amount=' +
      tempLimit_B; //, limitReversal.toString());
    l.info(`${this.constructor.name}`, side, limitReversal, url);
    (async function main() {
      try {
        return await axios.post(url);
        //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
      } catch (e) {
        console.error(e);
      }
    })();
  }
}

function doSmallShortAdapt() {}

function cancelOrders() {
  // var url =
  //  'http://'+ process.env.DOMAIN +'/api/mrgreen/cancelOrders/?apiKey=' +
  //  key +
  //  '&apiSecret=' +
  //  secret +
  //  '&symbol=XBTUSD';
  (async function main() {
    try {
      return await client.cancelAllOrders();
      //return await axios.post(url);
      //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
    } catch (e) {
      console.error(e);
    }
  })();
  orderPlaced = false;
}

function updateDBCheck(req, res) {
  checkRevs();
  currentQty = localStorage.getItem('currentQty');
  limitReversal = localStorage.getItem('limitReversal');
  currentSignal = localStorage.getItem('latestSignal');
  //console.log('update db check');
  //this is for if a buy or sell signal comes thru, this will execute adding it to DB
  if (updateDB === true) {
    var _buyRev = localStorage.getItem('_buy_rev');
    var _sellRev = localStorage.getItem('_sell_rev');
    console.log('updateDB = true', _buyRev, _sellRev);
    updateDB = false;
    // specify the database we are going to use
    var alice = nano.use('alice');
    // and insert a document in it
    if (currentSignal === 'Buy') {
      alice.insert({
        long: true,
        _rev: _buyRev
      }, 'buy', function (err, body) {
        if (err) {
          console.log('[alice.insert] ', err.message);
          updateDB = true;
          return;
        }
        console.log(
          'td has sent a buy order and this has been updated in the db.'
        );
        // console.log(body);
      });
    }
    if (currentSignal === 'Sell') {
      alice.insert({
        short: true,
        _rev: _sellRev
      }, 'sell', function (
        err,
        body
      ) {
        if (err) {
          console.log('[alice.insert] ', err.message);
          updateDB = true;
          return;
        }
        console.log(
          'td has sent a sell order and this has been updated in the db.'
        );
        console.log(body);
      });
    }
    updateDBStats = true;
  }
}

function updateCurrentStats(req, res) {
  checkRevs();

  limitReversal = localStorage.getItem('limitReversal');
  currentSignal = localStorage.getItem('latestSignal');
  //console.log('update db stats');
  //this is for if a buy or sell signal comes thru, this will execute adding it to DB
  if (updateDBStats === true) {
    var _statsRev = localStorage.getItem('_stats_rev');
    console.log('updateDBStats = true', _statsRev);
    updateDBStats = false;
    // specify the database we are going to use
    var stats = nano.use('stats');
    // and insert a document in it

    var _lsl = localStorage.getItem('_lsl');
    var _ssl = localStorage.getItem('_ssl');
    var _midprice = localStorage.getItem('_midprice');
    var _lslstart = localStorage.getItem('_lslstart');
    var _sslstart = localStorage.getItem('_sslstart');
    var _stbuylabel = localStorage.getItem('_stbuylabel');
    var _stselllabel = localStorage.getItem('_stselllabel');
    var _limitReversal = localStorage.getItem('limitReversal');
    var _currentSignal = localStorage.getItem('latestSignal');

    stats.insert({
        _id: 'stats',
        limitReversal: _limitReversal,
        currentSignal: _currentSignal,
        currentQty: currentQty,
        lsl: _lsl,
        ssl: _ssl,
        midprice: _midprice,
        lslstart: _lslstart,
        sslstart: _sslstart,
        _rev: _statsRev,
      },
      'stats',
      function (err, body) {
        if (err) {
          console.log('[stats.insert] ', err.message);
          updateDBStats = true;
          return;
        }
        console.log(body);
      }
    );
  }
}

function checkRevs() {
  var alice = nano.use('alice');
  alice.get('buy', {
    revs_info: true
  }, function (err, body) {
    if (!err) {
      localStorage.setItem('_buy_rev', body._rev);
      //console.log( 'body', body);
    }
  });
  alice.get('sell', {
    revs_info: true
  }, function (err, body) {
    if (!err) {
      localStorage.setItem('_sell_rev', body._rev);
      //console.log( 'body', body);
    }
  });

  var alice_stats = nano.use('stats');
  alice_stats.get('stats', {
    revs_info: true
  }, function (err, body) {
    if (!err) {
      localStorage.setItem('_stats_rev', body._rev);
      localStorage.setItem('currentQty', body.currentQty);
      //localStorage.setItem('currentSignal', currentSignal);
      localStorage.setItem('limitReversal', body.limitReversal);

      var __lsl = localStorage.getItem('_lsl');

      if (body.lsl > 0) {
        localStorage.setItem('_lsl', body.lsl);
        l.info('BODY.LSL ', body.lsl);
      }
      if (body.lslstart > 0) {
        localStorage.setItem('_lslstart', body.lslstart);
      }
      if (body.sslstart > 0) {
        localStorage.setItem('_sslstart', body.sslstart);
      }
      if (body.ssl) {
        localStorage.setItem('_ssl', body.ssl);
      }
    }
  });

  var alice_orders = nano.use('orders');
  alice_orders.get('latestOrder', {
    revs_info: true
  }, function (err, body) {
    if (!err) {
      localStorage.setItem('_orders_rev', body._rev);
    }
  });
}

function getLocalStats() {
  var _lsl = localStorage.getItem('_lsl');
  var _ssl = localStorage.getItem('_ssl');
  var _midprice = localStorage.getItem('_midprice');
  var _lslstart = localStorage.getItem('_lslstart');
  var _sslstart = localStorage.getItem('_sslstart');
  var _stbuylabel = localStorage.getItem('_stbuylabel');
  var _stselllabel = localStorage.getItem('_stselllabel');
  var _limitReversal = localStorage.getItem('limitReversal');
  var _currentSignal = localStorage.getItem('latestSignal');
}

function placeAdaptLimit() {
  //var _orders_rev = localStorage.getItem("_orders_rev");
  var _orders_rev = '1';
  var adapt = new adaptiveLimit(key, secret, 30, 'Buy');
  return adapt;
  //above in development still; for now, a simple fix taht allows us to continue work on the misfiring of orders
  var orders = nano.use('orders');
  orderPlaced = false;

  if (currentSignal === 'Buy') {
    //doFullLongAdapt();
    orders.insert({
        _id: 'latestOrder',
        _rev: _orders_rev,
        side: side
      },
      'orders',
      function (err, body) {
        if (err) {
          console.log('[orders.insert] ', err.message);
          updateDBStats = true;
          return;
        }
        l.info('1154', body);
      }
    );
  }
  if (currentSignal === 'Sell') {
    //doFullShortAdapt();
    orders.insert({
      _id: 'latestOrder',
      _rev: _orders_rev
    }, 'orders', function (
      err,
      body
    ) {
      if (err) {
        console.log('[stats.insert] ', err.message);
        updateDBStats = true;
        return;
      }
      l.info('1169', body);
    });
  }
}

function replaceWithAdaptLimit() {
  //var _orders_rev = localStorage.getItem("_orders_rev");
  var _orders_rev = '1';
  // var adapt = new adaptiveLimit(key, secret, 30, 'Buy');
  // return adapt;
  //above in development still; for now, a simple fix taht allows us to continue work on the misfiring of orders
  var orders = nano.use('orders');
  orderPlaced = false;

  if (currentSignal === 'Buy') {
    doFullLongAdapt();
    orders.insert({
        _id: 'latestOrder',
        _rev: _orders_rev,
        side: side
      },
      'orders',
      function (err, body) {
        if (err) {
          console.log('[orders.insert] ', err.message);
          updateDBStats = true;
          return;
        }
        l.info('1154', body);
      }
    );
  }
  if (currentSignal === 'Sell') {
    doFullShortAdapt();
    // orders.insert({ _id: 'latestOrder', _rev: _orders_rev }, 'orders', function(
    //   err,
    //   body
    // ) {
    //   if (err) {
    //   console.log('[stats.insert] ', err.message);
    //  updateDBStats = true;
    // return;
    //  }
    // l.info('1169', body);
  };
}


function logSignals() {
  signalCounter = signalCounter + 1;

  //l.info(    'counter since last signal update',    signalCounter,    currentSignal,    prevSignal  );
  if (currentSignal != prevSignal) {
    if (currentSignal != null) {
      if (prevSignal != null) {
        signalCounter = 0;
        l.info('update prevSignal');
        prevSignal = currentSignal;
        //placeAdaptLimit();
      }
    }
  }
}

function logGlobals() {
  //console.log(`Current directory: ${process.cwd()}`);
  var bidPrice = localStorage.getItem('bidPrice');
  var askPrice = localStorage.getItem('askPrice');
  var currentOrdStatus = localStorage.getItem("currentOrdStatus");
  var primaryOrderID = localStorage.getItem("primaryOrderID");
  var currentOrderQty = localStorage.getItem("currentOrderQty");

  unrealisedPnl = localStorage.getItem('unrealisedPnl');
  unrealisedPnlPcnt = localStorage.getItem('unrealisedPnlPcnt');
  unrealisedRoePcnt = localStorage.getItem('unrealisedRoePcnt');

  avgCostPrice = localStorage.getItem('avgCostPrice');
  avgEntryPrice = localStorage.getItem('avgEntryPrice');
  breakEvenPrice = localStorage.getItem('breakEvenPrice');

  localStorage.setItem('apiKey', key);
  localStorage.setItem('apiSecret', secret);


  console.log(
    ' 1197 - ',
    primaryOrderID,
    currentOrdStatus,
    currentOrderQty,
    globals.updatePrimaryOrderID,
    globals.adjustPrimaryOrder,
    globals.primaryTargets,
    globals.adjustPrimaryTargets,
    askPrice,
    bidPrice,
    unrealisedPnl,
    unrealisedPnlPcnt,
    unrealisedRoePcnt,
    avgCostPrice,
    avgEntryPrice,
    breakEvenPrice


  );

  // console.log(globals.setPrimaryOrderID('weeerd'));
  //client.instrument();
}

function updateOrderInfo() {
  var url =
    'http://mrgreenswag.ddns.net/api/mrgreen/getOrders/?apiKey=' +
    key +
    '&apiSecret=' +
    secret; //, limitReversal.toString());
  // console.log(url);
  (async function main() {
    try {

      //client.getOrders(true)
      orderLogic();
      //return await axios.get(url);
      //return await axios.get('http://mrgreenswag.ddns.net/api/mrgreen/adaptReversal/?apiKey=ZlFxoiWlyIZuLCa4nnkApVAo&apiSecret=i7UYkphhtL6dk1RnA3vaPFnURt0gSfGUMHetyf58wrwpxvTA&side=', side ,'&amount=', limitReversal )
    } catch (e) {
      console.error(e);
    }
  })();


}

function orderLogic() {
  var orders1 = localStorage.getItem('currentOrders');
  var orders2 = localStorage.getItem('primaryOrderID');
  //localStorage.setItem('currentOrders', 'weeerd');
  //console.log(' - - -- -', orders1, orders2)
  primaryOrderID = orders1;
  if (orders1 !== 'weeerd') {
    checkIfPrimaryNeedsAdjustment();
    //l.info('PRIMARY ORDER FOUND ', orders1);
  }
  if (orders1 === 'weeerd') {
    console.log('no primary order id');
    updatePrimaryOrderID = true;
    adjustPrimaryOrder = false;
    localStorage.setItem("currentOrderPrice", 0);
    //localStorage.setItem("currentOrders", 'weeerd');
    //localStorage.setItem("primaryOrderID", 'weeerd');
    //localStorage.setItem("currentOrdStatus", 'none');
  }
  if (updatePrimaryOrderID === true) {
    checkForPrimaryOrderID();
    updatePrimaryOrderID = false;
    // console.log('globals.updatePrimaryOrderID = false;');
  }
  if (adjustPrimaryOrder === true) {
    if (primaryOrderID !== '') {
      console.log('should check for primary adjustment');
      checkIfPrimaryNeedsAdjustment();
    }
    if (adjustPrimaryOrder === true) {
      //amendPrimaryOrder();
      console.log('should check for mary adjustment');
    }
  }
}

function checkForPrimaryOrderID() {

  if (updatePrimaryOrderID === true) {
    client.getOrders(true);
    updatePrimaryOrderID = false;
    var orders1 = localStorage.getItem('currentOrders');
    if (orders1 !== 'weeerd') {
      primaryOrderID = orders1;
      console.log('PRIMARY ORDER ID SET TO: ', primaryOrderID);
    }
  }
}

function checkIfPrimaryNeedsAdjustment() {
  var currentOrderPrice = localStorage.getItem('currentOrderPrice');
  bidPrice = localStorage.getItem('bidPrice');
  askPrice = localStorage.getItem('askPrice');
  currentSignal = latestSignal;
  //console.log(    'CHECKIFPRIMARYNEEDSADJUSTMENT:',    currentOrderPrice,    bidPrice,    askPrice,    currentSignal);
  if (currentSignal === 'Buy') {
    //console.log('======================== LONG W ', askPrice, bidPrice);
    if (currentOrderPrice < bidPrice) {
      if (adjustPrimaryOrder === true) {
        adjustPrimaryOrder = false;
        amendPrimaryOrder();
      }
    }
  }
  if (currentSignal === 'Sell') {
    //console.log(':::::::::::::::::::::::::::::: SHORT W ', currentOrderPrice, askPrice, bidPrice);

    //console.log('xxxxxxxxxxxxxxxxxxxx- prices found -xxxxxxxxxxxxxxxxx');
    if (adjustPrimaryOrder === true) {
      adjustPrimaryOrder = false;
      amendPrimaryOrder();
    }

  }
}

function amendPrimaryOrder() {
  //console.log( '--------------- ----       amendPrimaryORder function called   -------------' );
  var primaryOrderID = localStorage.getItem('primaryOrderID');
  var orderQty = localStorage.getItem('orderQty');
  var bidPrice = localStorage.getItem('bidPrice');
  var askPrice = localStorage.getItem('askPrice');

  if (currentSignal === 'Buy') {
    //console.log('dddddddddddd', primaryOrderID);
    // amendCounterIncrease();
  }
  if (currentSignal === 'Sell') {
    //console.log('bbbbbbbbbbbb', primaryOrderID);
    //amendCounterIncrease();
  }
}

function amendCounterIncrease() {

  amendCounter = amendCounter + 1;
  var currentOrdStatus = localStorage.getItem("currentOrdStatus");
  console.log(amendCounter);
  if (amendCounter >= 99) {
    (async function () {
      // updateInstrument();
      try {
        //need to swap and have this true when signals are recieved and placed by wolfbot
        replaceOrder = true;

        if (replaceOrder === true) {
          replaceOrder = false;
          if (amendCounter >= 100) {
            l.info('--------------------------')
            var orderQty = localStorage.getItem('currentOrderQty')
            var orderID = localStorage.getItem('primaryOrderID')
            var signal = latestSignal
            var currentOrderPrice = localStorage.getItem("currentOrderPrice");
            var askPrice = 0;
            var bidPrice = 0;
            var orderPrice = 0;

            askPrice = localStorage.getItem('askPrice')
            bidPrice = localStorage.getItem('bidPrice')

            if (signal === 'Buy') {
              if (currentOrderPrice > askPrice) {
                orderPrice = askPrice;
              } else {
                orderPrice = bidPrice;
              }
            }
            if (signal === 'Sell') {
              if (currentOrderPrice < bidPrice) {
                orderPrice = bidPrice;
              } else {
                orderPrice = askPrice;
              }
            }
            l.info('UPDATE ORDER DISABLED - 1408 ', orderQty, orderID, orderPrice, signal)
            //client.updateOrder(orderID, orderQty, orderPrice)
            if (newPrimaryOrderPlaced === true) {
              if (primaryOrderID !== 'weeerd') {
                if (currentOrderPrice > askPrice) {
                  const primaryOrder = new PrimaryOrder('Sell');
                }
                if (currentOrderPrice < bidPrice) {
                  const primaryOrder = new PrimaryOrder('Buy');
                }

                async () => {
                  try {

                    return await primaryOrder.do();
                  } catch (error) {
                    console.error(error);
                  }
                };
              }
            }

            //return await axios.post('http://localhost:3000/api/mrgreen/updatePrimaryOrder' )
            //cancelOrders();
            //ocalStorage.setItem('doReversal', true);
          }
          if (amendCounter >= 101) {
            amendCounter = 0;
            // replaceWithAdaptLimit();
          }
        }



        l.info('AAAAAA function amendCounterIncrease -  adapt limit order disabled until logic for cancelling order and issuing re order is mature')

      } catch (e) {
        console.error(e);
      }
    })();
  };

  if (amendCounter < 99) {
    if (amendCounter > 0) {
      //  console.log(' amendcounter within rnage ');
      if (amendCounter > 1) {
        amendCounter = 100;
      }
    }
  }
}

function updateInstrument() {
  //  client.instrument();
}

function primaryOrderLogic() {
  
  var priceAction = '';
  var status = '';

  var bidPrice = localStorage.getItem('bidPrice');
  var askPrice = localStorage.getItem('askPrice');
  var currentOrdStatus = localStorage.getItem("currentOrdStatus");
  currentOrderPrice = localStorage.getItem("currentOrderPrice");


  //l.info('1484- ', adjustPrimaryOrder, currentOrdStatus, currentOrderPrice, currentOrderQty, currentOrderFilled, currentOrderCancel, currentOrderComplete);

  if (currentOrderComplete === false) {



    if (primaryOrderID !== 'weeerd') {

      //IS ORDER FOUND
      if (currentOrdStatus === 'New') {
        if (currentOrderPrice !== 0) {
          //check price; set adjust to true
          if (currentOrderPrice > askPrice) {
            adjustPrimaryOrder = true
          } //else
          if (currentOrderPrice < bidPrice) {
            adjustPrimaryOrder = true
          } //else
          //{ adjustPrimaryOrder = false      }
        }
      }
    }

    //IS ORDER FLLED
    if (currentOrdStatus === 'Filled') {
      adjustPrimaryOrder = false;
      l.info('//check price XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
      //set adjust to true
      localStorage.setItem('currentOrders', 'weeerd');
      localStorage.setItem('primaryOrderID', 'weeerd');
      updatePrimaryOrderID = true;
      takeProfit();
    }

    //IS ORDER CANCELLED
    if (currentOrdStatus === 'Canceled') {
      console.log('//reissue order for same dilly');
      adjustPrimaryOrder = false;
      localStorage.setItem('currentOrders', 'weeerd');
      localStorage.setItem('primaryOrderID', 'weeerd');
      updatePrimaryOrderID = true;
    }
    if (adjustPrimaryOrder === true) {
      amendCounterIncrease();
      adjustPrimaryOrder = false;
      // localStorage.setItem("currentOrderPrice", 0);
      //localStorage.setItem("currentOrders", 'weeerd');
      //localStorage.setItem("primaryOrderID", 'weeerd');
      //localStorage.setItem("currentOrdStatus", 'none');
    }

  }
}
//function wsCheck(){
//  if( )
//}
setInterval(syncDB, 15000);

setInterval(primaryOrderLogic, 750)
//setInterval(wsCheck, 2000);

//setInterval(amendCounterIncrease, 1000);
//setInterval(updateInstrument, 8000);
setInterval(updateDBCheck, 10000);
setInterval(updateCurrentStats, 10000);
setInterval(statusCheck, 10000);
setInterval(logSignals, 11000);
setInterval(logGlobals, 11000);


//setInterval(positionCheck, 30000);
setInterval(guardCheck, 10000);
setInterval(cancelCheck, 10000);
setInterval(newOrderCheck, 36000);
//setInterval(updateOrderInfo, 13000);
setInterval(updateOrderInfo, 13000)
//setInterval(doFullLongAdapt, 8000);


getLocalStats();
localStorage.setItem('currentOrders', 'weeerd');

localStorage.setItem('apiKey', key)
localStorage.setItem('apiSecret', secret)

const wsInstance = new ws;



export default new MrGreenDatabaseST();