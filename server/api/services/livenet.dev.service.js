const crypto = require('crypto');
//const qs = require('qs');
const fetch = require('node-fetch');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 4000;
const path = require('path');

var cors = require('cors');
var fs = require('fs-extra');
let unix_timestamp = 0;
let formattedTime = 0;

var throttledQueue = require('throttled-queue');
global.requests_per_interval = 1;
global.interval = 1200;
global.throttle = throttledQueue(requests_per_interval, interval, true);
global.time_started = Date.now();
global.max_rpms = requests_per_interval / interval;
global.num_requests = 20;
global.request_limit = 100;
global.userRequests = 0;

global.seconds = 0;
global.secondsReset = false;
global.secondsSwitch = false;

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//get latest order info from cmf indicator
app.get('/api/reverseSignalTrendASIADMF', function(req, res) {
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var directionUrl = req.query.direction;

  //insert code here
  var msg = 'CMF SIGNAL - TREND';
  console.log(msg);

  var thing = Math.floor(Date.now() / 1000);
  console.log(thing);

  var myDate = new Date(thing * 1000);
  thing = myDate.toLocaleString() + ' direction ' + directionUrl + ' <br/> ';
  console.log(thing);

  // Import fs module.
  var fs = require('fs');

  // Convert JSON format string to js object.
  function stringToObject(JSONString) {
    var jsonObject = JSON.parse(JSONString);

    console.log('User Name : ' + jsonObject.user_name);

    console.log('Password : ' + jsonObject.password);

    console.log('Email : ' + jsonObject.email);

    // Change email property value in JS object.
    jsonObject.email = 'jerry@dev2qa.com';

    // Add qq and phone property in JS object.
    jsonObject.qq = '111111';
    jsonObject.phone = '13901234567';

    return jsonObject;
  }

  // Convert javascript object to json string.
  function objectToString(jsObject) {
    var jsonString = JSON.stringify(jsObject);

    console.log('New JSON String : ' + jsonString);

    return jsonString;
  }

  // Write json string to json file.
  function writeJSONStringToFile(jsonString, filePath) {
    // Create a write stream.
    var writeStream = fs.createWriteStream(filePath, {
      flags: 'a',
    });

    writeStream.write(jsonString);

    writeStream.end('');
  }

  // Read json object from a json text file.
  function jsonFileToArray(filePath) {
    // This code will get a json object from a .json file.
    var jsonObject = require(filePath);

    // Get color json object array.
    var colorArray = jsonObject.colors;

    var arraySize = colorArray.length;

    for (var i = 0; i < arraySize; i++) {
      var colorItem = colorArray[i];

      var color = colorItem.color;

      var category = colorItem.category;

      var type = colorItem.type;

      var code = colorItem.code;

      var rgba = code.rgba;

      var hex = code.hex;

      console.log(
        'Color : ' +
          color +
          ' , Category : ' +
          category +
          ' , Type : ' +
          type +
          ' , Code Rgba : ' +
          rgba +
          ' , Hex : ' +
          hex
      );
    }
  }

  //end code inject

  (async function main() {
    try {
      // Create a new JSON string.
      var jsonString =
        '{"user_name" : "Jerry Zhao", "password" : "888888", "email" : ["abc@gmail.com, def@dev2qa.com"]}';

      // Get the json object by above string.
      var jsonObject = stringToObject(jsonString);

      // Change the js object property and convert to json string again.
      var newJsonString = objectToString(jsonObject);

      // Write the new json string to a json file.
      writeJSONStringToFile(thing, 'trend.json');

      // Parse json data from a json file.
      jsonFileToArray('./asiadmf.json');
      res.send(apiKey);
    } catch (e) {
      console.error(e);
    }
  })();
});

app.get('/api/testApiIncludes/', function(req, res) {
//    var apiKey = req.query.apiKey;
 //   var apiSecret = req.query.apiSecret;
    var fs = require('fs');
  
    (async function main() {
      try {
        const shell = require('shelljs');
  
 //       shell.exec('sudo sh ./loadAccount02.sh');

        var apiKey = 'ksjhfdhj';
        res.send(apiKey);
      } catch (e) {
        console.error(e);
      }
    })();
  });


function makeRequest(apiKeyPassed, apiSecretPassed, verb, endpoint, data = {}) {
    const apiRoot = '/api/v1/';
    var apiKey = apiKeyPassed;
    var apiSecret = apiSecretPassed;
    const expires = Math.round(new Date().getTime() / 1000) + 60; // 1 min in the future
  
    let query = '',
      postBody = '';
    if (verb === 'GET') query = '?' + qs.stringify(data);
    // Pre-compute the reqBody so we can be sure that we're using *exactly* the same body in the request
    // and in the signature. If you don't do this, you might get differently-sorted keys and blow the signature.
    else postBody = JSON.stringify(data);
  
    const signature = crypto
      .createHmac('sha256', apiSecret)
      .update(verb + apiRoot + endpoint + query + expires + postBody)
      .digest('hex');
  
    const headers = {
      'content-type': 'application/json',
      accept: 'application/json',
      // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
      // https://www.bitmex.com/app/apiKeysUsage for more details.
      'api-expires': expires,
      'api-key': apiKey,
      'api-signature': signature,
    };
  
    const requestOptions = {
      method: verb,
      headers,
    };
    if (verb !== 'GET') requestOptions.body = postBody; // GET/HEAD requests can't have body
    const url = 'https://www.bitmex.com' + apiRoot + endpoint + query;
  
    return fetch(url, requestOptions)
      .then(response => response.json())
      .then(
        response => {
          //num_requests = num_requests -1;
          if ('error' in response) throw new Error(response.error.message);
          return response;
        },
        error => console.error('Network error', error)
      );
  }
  
  app.listen(port, () => console.log(`Listening on port ${port}`));
  