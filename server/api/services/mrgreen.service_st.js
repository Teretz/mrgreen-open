import l from '../../common/logger';
//import db from './mrgreen.db.service';
import db_st from './mrgreen.db_st.service';

let testVal = 'return works :)';
class MrGreenServiceST{
  
  instrumentDev(){
    l.info(`${this.constructor.name}.instrument()`);
    return db_st.instrument()
  }
  updateSuperTrendInfo(lsl, ssl, midprice, lslstart, sslstart, stbuylabel, stselllabel){
    l.info(`${this.constructor.name}.updateSuperTrendInfo()`);
    console.log('prev :', global.prevLSL,  global.prevSSL, global.currentLSL, global.prevSSL,);
    return db_st.updateSuperTrendInfo(lsl, ssl, midprice, lslstart, sslstart, stbuylabel, stselllabel);
  }
  updateDBStats(){
    l.info(`${this.constructor.name}.updateDBStats()`);
    console.log('prev :', global.prevLSL,  global.prevSSL, global.currentLSL, global.prevSSL,);
    return db_st.updateSuperTrendInfo();
  }
  getPositionInfo(apiKey, apiSecret){
    return db_st.getPositionInfo(apiKey, apiSecret)
  }
  shortStopLossUpdate(sslUpdate){
    return db_st.updateSuperTrendInfo(sslUpdate)
  }
  shortStopLossStart(sslStart){
    return db_st.getShortStopLossStart(sslStart)
  }  
  longStopLossUpdate(lslUpdate){
    return db_st.updateSuperTrendInfo(lslUpdate)
  }
  longStopLossStart(lslStart){
    return db_st.updateSuperTrendInfo(lslStart)
  }

  setLimitLongs(limitLongs){
    return db_st.setLimitLongs(limitLongs)
  }
  setLimitShorts(limitShorts){
    return db_st.setLimitShorts(limitShorts)
  }
  setLimitReversal(limitReversal){
    return db_st.setLimitReversal(limitReversal)
  }

  placeWolfOrder(){
    return db_st.placeWolfOrder()
  }

  
setSignalLong(signal, time){    return db_st.setSignalLong(signal, time)

  }

setSignalShort(signal, time){    return db_st.setSignalShort(signal, time)

}

}

export default new MrGreenServiceST();
