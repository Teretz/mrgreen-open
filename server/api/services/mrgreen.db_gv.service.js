var express = require('express')
  , app     = express()
  , PouchDB = require('pouchdb');

app.use('/api/mrgreen', require('express-pouchdb')(PouchDB));

//var myPouch = new PouchDB('foo');

let db = new PouchDB('guard_value');
//let db_gv = new PouchDB('guard_value');

let remoteCouch = 'http://localhost:5984/todos';
let remoteCouch_gv = 'http://localhost:5984/guard_value';
let remoteCouch_gd = 'http://localhost:5984/guard_direction';

class MrGreenDatabaseGV {
  constructor() {
    this._data = [];
    this._counter = 0;

    this.insert('exa34mple 0');
    this.insert('exa34mple 1');
  }



  all() {
    return Promise.resolve(this._data);
  }

  test(val) {
    //return Promise.resolve('alchemy -'+ val +' ._o ');
    return Promise.resolve(val.toString());
  }

  byId(id) {
    return Promise.resolve(this._data[id]);
  }
  addTodo(val) {
    //console.log(val);
    //var text = val;
    var todo = {
      _id: new Date().toISOString(),
      title: val,
      completed: false
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted a todo!');
        //sync();
      }
    });
    return Promise.resolve(val.toString());
  }

  setGuardValue(guardValue) {
    //console.log(val);
    //var text = val;
    var todo = {
      _id: new Date().toISOString(),
      title: guardValue,
      guardValue: guardValue,
      completed: false
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted guard value!');
        //sync();
      }
    });
    
    return Promise.resolve(guardValue);
  }

  setGuardDirection(guardDirection) {
    //console.log(val);
    //var text = val;
    var todo = {
      _id: new Date().toISOString(),
      title: guardDirection,
      guardDirection: guardDirection,
      completed: false
    };
    db.put(todo, function callback(err, result) {
      if (!err) {
        console.log('Successfully posted guard direction change!');
        //sync();
      }
    });
    
    return Promise.resolve(guardDirection);
  }


  showTodos() {
    db.allDocs({include_docs: true, descending: true}, function(err, doc) {
      if (!err) {
        console.log('Successfully called to show all todo!');
        redrawTodosUI(doc.rows);
      }
    });
  }

  

  insert(name) {
    const record = {
      id: this._counter,
      name,
    };

    this._counter += 1;
    this._data.push(record);

    return Promise.resolve(record);
  }

  checkboxChanged(todo, event) {
    todo.completed = event.target.checked;
    db.put(todo);
  }

  deleteButtonPressed(todo) {
    db.remove(todo);
  }

}

// Given an object representing a todo, this will create a list item
    // to display it.
    function createTodoListItem(todo) {
      var checkbox = document.createElement('input');
      checkbox.className = 'toggle';
      checkbox.type = 'checkbox';
      checkbox.addEventListener('change', checkboxChanged.bind(this, todo));
  
      var label = document.createElement('label');
      label.appendChild( document.createTextNode(todo.title));
      label.addEventListener('dblclick', todoDblClicked.bind(this, todo));
  
      var deleteLink = document.createElement('button');
      deleteLink.className = 'destroy';
      deleteLink.addEventListener( 'click', deleteButtonPressed.bind(this, todo));
  
      var divDisplay = document.createElement('div');
      divDisplay.className = 'view';
      divDisplay.appendChild(checkbox);
      divDisplay.appendChild(label);
      divDisplay.appendChild(deleteLink);
  
      var inputEditTodo = document.createElement('input');
      inputEditTodo.id = 'input_' + todo._id;
      inputEditTodo.className = 'edit';
      inputEditTodo.value = todo.title;
      inputEditTodo.addEventListener('keypress', todoKeyPressed.bind(this, todo));
      inputEditTodo.addEventListener('blur', todoBlurred.bind(this, todo));
  
      var li = document.createElement('li');
      li.id = 'li_' + todo._id;
      li.appendChild(divDisplay);
      li.appendChild(inputEditTodo);
  
      if (todo.completed) {
        li.className += 'complete';
        checkbox.checked = true;
      }
  
      return li;
    }
  
    function redrawTodosUI(todos) {
      var ul = document.getElementById('todo-list');
      ul.innerHTML = '';
      todos.forEach(function(todo) {
        ul.appendChild(createTodoListItem(todo.doc));
      });
    }
  
    showTodos();  

    if (remoteCouch_gv) {
      sync_gv();
    }

function sync_gv() {
  //syncDom.setAttribute('data-sync-state', 'syncing');
  var opts = {live: true};
  db.replicate.to(remoteCouch_gv, opts, syncError);
  db.replicate.from(remoteCouch_gv, opts, syncError);
}
function sync_gd() {
  //syncDom.setAttribute('data-sync-state', 'syncing');
  var opts = {live: true};
  db.replicate.to(remoteCouch_gd, opts, syncError);
  db.replicate.from(remoteCouch_gd, opts, syncError);
}
function syncError() {
  //syncDom.setAttribute('data-sync-state', 'error');
}

function showTodos() {
  db.allDocs({include_docs: true, descending: true}, function(err, doc) {
    if (!err) {
      console.log('Successfully called to show all todo!');
    }
  });
}

export default new MrGreenDatabaseGV();
