import l from '../../common/logger';
import db from './mrgreen.db.service';


let testVal = 'return works :)';
class MrGreenService {
  
  all() {
    l.info(`${this.constructor.name}.all()`);
    return db.all();
  }

  byId(id) {
    l.info(`${this.constructor.name}.byId(${id})`);
    return db.byId(id);
  }

  create(name) {
    return db.insert(name);
  }

  test(val){
    l.info(`${this.constructor.name}.test()`);
    return db.test(val);
    
  }
  addTodo(val){
    l.info(`${this.constructor.name}.addTodo()`);
    return db.addTodo(val);
    
  }
  setGuardValue(guardValue){
    l.info(`${this.constructor.name}.setGuardValue()`);
    return db_gv.setGuardValue(guardValue);
    
  }
  setGuardDirection(guardDirection){
    l.info(`${this.constructor.name}.setGuardDirection()`);
    return db_gd.setGuardDirection(guardDirection);
  }


}

export default new MrGreenService();
