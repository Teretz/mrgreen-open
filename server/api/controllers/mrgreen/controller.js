import ExamplesService from '../../services/examples.service';
import MrGreenService from '../../services/mrgreen.service';
import MrGreenServiceGV from '../../services/mrgreen.service_gv';
import MrGreenServiceGD from '../../services/mrgreen.service_gd';
import MrGreenServiceST from '../../services/mrgreen.service_st';
import l from '../../../common/logger';
const env = require('dotenv');
var func = require('../../../mr-greens-scripts/functions');

let localStorage = null;

const crypto = require('crypto');
const qs = require('qs');
const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const path = require('path')
var request = require('request');

const clientPath = path.join(process.cwd() , '/server/node-bitmex/Client');
const Client = require(clientPath);
const key = process.env.API_KEY;
const secret = process.env.API_SECRET;
const apiKeyTrade = key;
const apiSecretTrade = secret;
const client = new Client(key, secret);
const SwaggerClient = require("swagger-client");
//const BitMEXAPIKeyAuthorization = path.join(process.cwd() + '/server/node-bitmex/lib/BitMEXAPIKeyAuthorization');




const manager = new SwaggerClient({
  url: 'https://www.bitmex.com/api/explorer/swagger.json',
  usePromise: true
});

let _guardUpdateCounter = 0;
let guardCounter = 0;

let pos = null;
let currentQty=0;
let currentOrderId='----';

let shortAmount =0;
let longAmount =0;

let shortSL = 0;
let longSL = 0;

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

app.use(express.json())

export class Controller {
  all(req, res) {
    MrGreenService.all().then(r => res.json(r));
  }

  byId(req, res) {
    MrGreenService.byId(req.params.id).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  create(req, res) {
    MrGreenService.create(req.body.name).then(r =>
      res
      .status(201)
      .location(`/api/mrGreen/${r.id}`)
      .json(r)
    );

  }
  mrGreenTest(req, res) {
    //MrGreenService.all().then(r => res.json(r));
    MrGreenService.test(req.params.val).then(r => res.json(r));

  }
  addTodo(req, res) {
    //MrGreenService.all().then(r => res.json(r));
    MrGreenService.addTodo(req.params.val).then(r => res.json(r));

  }
  addGuardValue(req, res) {
    //MrGreenService.all().then(r => res.json(r));
    MrGreenServiceGV.setGuardValue(req.params.guardValue).then(r => res.json(r));
  }
  setGuardDirection(req, res) {
    //MrGreenService.all().then(r => res.json(r));
    MrGreenServiceGD.setGuardDirection(req.params.guardDirection).then(r => res.json(r));

  }
  updateSuperTrendInfo(req, res) {
    l.info("DSFGHGDHGFH", req.body.lsl, req.body.ssl, req.body.midprice);
    const midprice = req.body.midprice;
    //MrGreenService.all().then(r => res.json(r));

    /* localStorage.setItem("_lsl", req.body.lsl,);
    localStorage.setItem("_ssl", req.body.ssl,);
    localStorage.setItem("_midprice", req.body.midprice);
    localStorage.setItem("_lslstart", req.body.lslstart);
    localStorage.setItem("_sslstart", req.body.sslstart);
    localStorage.setItem("_stbuylabel", req.body.stbuylabel); 
    localStorage.setItem("_stselllabel", req.body.stselllabel);
*/
    MrGreenServiceST.updateSuperTrendInfo(req.body.lsl, req.body.ssl, req.body.midprice, req.body.lslstart, req.body.sslstart, req.body.stbuylabel, req.body.stselllabel).then(r => res.json(r));
  }
  updateStats(req, res) {
    l.info("DSsssssss", req.body.lsl, req.body.ssl, req.body.midprice);
    const midprice = req.body.midprice;
    //MrGreenService.all().then(r => res.json(r));

    /* localStorage.setItem("_lsl", req.body.lsl,);
    localStorage.setItem("_ssl", req.body.ssl,);
    localStorage.setItem("_midprice", req.body.midprice);
    localStorage.setItem("_lslstart", req.body.lslstart);
    localStorage.setItem("_sslstart", req.body.sslstart);
    localStorage.setItem("_stbuylabel", req.body.stbuylabel); 
    localStorage.setItem("_stselllabel", req.body.stselllabel);
*/
    MrGreenServiceST.updateDBStats(req.body.lsl, req.body.ssl, req.body.midprice, req.body.lslstart, req.body.sslstart, req.body.stbuylabel, req.body.stselllabel).then(r => res.json(r));
  }
  shortStopLossStart(req, res) {
    console.log("SSL START", req.params.sslStart); //req.params.ssl );
    MrGreenServiceST.shortStopLossStart(req.params.sslStart).then(r => res.json(r));
  }
  shortStopLossUpdate(req, res) {
    console.log("SSL UPDATE", req.params.ssl);
    MrGreenServiceST.shortStopLossUpdate(req.params.ssl).then(r => res.json(r));
  }
  longStopLossStart(req, res) {
    console.log("LSL START", req.params.lslStart);
    MrGreenServiceST.longStopLossUpdate(req.params.lslStart).then(r => res.json(r));
  }
  longStopLossUpdate(req, res) {

    console.log("LSL UPDATE", req.params.lsl);
    MrGreenServiceST.longStopLossUpdate(req.params.lslUpdate).then(r => res.json(r));
  }
  instrumentDev(req, res) {
    (async function main() {
      try {
        let result = makeRequest(key, secret, 'GET', 'order', {
          symbol: "XBTUSD",
          orderQty: 30,
          price: 8200,
          ordType: "Stop",
          side: "Buy"
        })
        console.log(res.currentQty);
        res.send({
          express: result
        });
      } catch (e) {
        console.error(e);
      }
    }());
  }
  shortStop(req, res) {
    (async function main() {
      try {
        let result = makeRequest(key, secret, 'GET', 'order', {
          symbol: "XBTUSD",
          orderQty: 30,
          price: 8200,
          ordType: "Stop",
          side: "Buy"
        })
        res.send({
          express: result
        });
      } catch (e) {
        console.error(e);
      }
    }());
  }
  getShortStopLoss(req, res) {
    var shortStop = localStorage.getItem("_ssl");
    console.log('shortstop is: ', shortStop);
  };
  getLongStopLoss(req, res) {
    var longStop = Item("_lsl");
    console.log('longstop is: ', longStop);
  };
  //place new order
  placeOrder(req, res) {
    //func.doAdaptiveLimit();

    var apiKey = req.query.apiKey;
    var apiSecret = req.query.apiSecret;
    var symbolUrl = req.query.symbol;
    var orderQtyUrl = req.query.orderQty;
    var priceUrl = req.query.price;
    var ordTypeUrl = req.query.ordType;
    var execInstUrl = req.query.execInst;
    var clOrdIdUrl = req.query.clOrdId;
    var stopPxUrl = req.query.stopPx;

    (async function main() {
      try {
        /*const result = await makeRequest('GET', 'position', {
                filter: { symbol: 'XBTUSD' },
                columns: ['currentQty', 'avgEntryPrice'],
              });*/
        const result = await makeRequest(apiKey, apiSecret, 'POST', 'order', {
          symbol: symbolUrl,
          orderQty: orderQtyUrl,
          price: priceUrl,
          ordType: ordTypeUrl,
          clOrdId: clOrdIdUrl,
          execInst: execInstUrl,
          stopPx: stopPxUrl,
        });
        res.send({
          express: result
        });
      } catch (e) {
        console.error(e);
      }
    })();
  };
  getPositionInfo(req, res) {
    var apiKey = req.query.apiKey;
    var apiSecret = req.query.apiSecret;
    var symbolUrl = req.query.symbol;
    (async function() {
      try {
         //const result = await getPosition(apiKey, apiSecret, 'GET', '/position', {
        //  filter: '{"symbol": "' + symbolUrl + '"}'
        //});
        //localStorage.setItem("qty", 0);
        var t2 = localStorage.getItem("qty");
        //var t1 = await client.position();
        //''t1 = JSON.parse(t1);
        
        //l.info(typeof client,  t1, key, secret, '><><><><><><><><><><>  ',t2, t1,' <><><><><><');
       
        res.send({
       //   express: t1
        });
        //console.log('CONTROLLER.js - line 239 ',pos,  result, result, res)
        //localStorage.setItem("currentQty", pos[0].currentQty);
      } catch (e) {
        console.error(e);
      };
    }());
  }
  getOrders(req, res) {
    console.log(' getttttting orders');
    var apiKey = req.query.apiKey;
    var apiSecret = req.query.apiSecret;
    var symbolUrl = req.query.symbol;
    (async function() {
      try {
        var t2 = localStorage.getItem("qty");
  //      var t1 = await client.getOrders(true);
        //l.info(`${this.constructor.name}`, typeof client, key, secret, '><><><><><><><><><><>  ',t2, t1,' <><><><><><');
        res.send({
     //     express: t1
        });
      } catch (e) {
        console.error(e);
      };
    }());
  }
  wolfPlaceOrder(req, res) {
    //console.log("LSL START", req.params.lslStart);
    MrGreenServiceST.longStopLossUpdate(req.params.lslStart).then(r => res.json(r));
  }
  setSignalLong(req, res) {
    l.info("BUY SIGNAL");
    localStorage.setItem("latestSignal", "Buy");
    var t3 = localStorage.setItem("signalTimeReceived", Math.round(new Date().getTime() / 1000) + 60);
    MrGreenServiceST.setSignalLong('Buy', t3)//.then(r => res.json(r));
    res.sendStatus(200);

  }
  setSignalShort(req, res) {
    l.info("SELL SIGNAL");
    localStorage.setItem("latestSignal", "Sell");
    var t4 = localStorage.setItem("signalTimeReceived", Math.round(new Date().getTime() / 1000) + 60);
    MrGreenServiceST.setSignalShort("Sell", t4)//.then(r => res.json(r));
    res.sendStatus(200);

  }
  

  setLimitLongs(req, res) {
    var limitLongs = req.params.limitLongs;
    l.info("limitlongs: ", limitLongs);
    localStorage.setItem("limitLongs", limitLongs);
    //localStorage.setItem("signalTimeReceived", Math.round(new Date().getTime() / 1000) + 60);
    MrGreenServiceST.setLimitLongs(limitLongs).then(r => res.json(r));
    res.sendStatus(200);
  }
  setLimitShorts(req, res) {
    var limitShorts = req.params.limitShorts;
    l.info("limitshorts: ", limitShorts);
    localStorage.setItem("limitShorts", limitShorts);
    //localStorage.setItem("signalTimeReceived", Math.round(new Date().getTime() / 1000) + 60);
    MrGreenServiceST.setLimitShorts(limitShorts).then(r => res.json(r));
    res.sendStatus(200);
  }
  setLimitReversal(req, res) {
    var limitReversal = req.params.limitReversal;
    l.info("limitReversal: ", limitReversal);
    localStorage.setItem("limitReversal", limitReversal);
    //localStorage.setItem("signalTimeReceived", Math.round(new Date().getTime() / 1000) + 60);
    MrGreenServiceST.setLimitReversal(limitReversal).then(r => res.json(r));
    res.sendStatus(200);
  }
  cancelOrders(req, res){
    var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var symbolUrl = req.query.symbol;


  (async function main() {
    try {
    //  const result = await cancelOrders(apiKey, apiSecret, 'GET', '/order/cancelAllAfter', {
    //    symbol: symbolUrl,
    //    timeout: '100'
    //  });
     client.cancelOrders();
      res.send({
    //    express: result
      });
    } catch (e) {
      console.error(e);
    };
  }());
 
  }
  
updatePrimaryOrder(){
  l.info('--------------------------')
var orderQty = localStorage.getItem('currentOrderQty')
var orderID = localStorage.getItem('primaryOrderID')
var signal = localStorage.getItem('currentSignal')
var orderPrice = '';
//if(signal === 'Buy'){
//   orderPrice = localStorage.getItem('askPrice')
//}
//if(signal === 'Sell'){
//   orderPrice = localStorage.getItem('bidPrice')
//}
l.info(orderID, orderQty, orderPrice)
client.updateOrder(orderID, orderQty, orderPrice)
}

  adaptReversal(req, res){
    var doReversal = localStorage.getItem("doReversal");
    l.info('controller.js/adaptReversal - is do rveersal true? ', doReversal)
    if(doReversal === true){
      localStorage.setItem("doReversal", false)
    console.log('adaptReversal --------------------')
    var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var symbolUrl = req.query.symbol;
  var sideUrl='';
var fs = require('fs');
  var apiKey = req.query.apiKey.toString();
  var apiSecret = req.query.apiSecret.toString();
  var signal = localStorage.getItem("latestSignal")
  var amountUrl = req.query.amount;
  if(signal==='Buy'){
    sideUrl='Buy'
  }
  if(signal==='Sell'){
    sideUrl='Sell'
  }

  (async function main() {

    try {
      const shell = require('shelljs')
      console.log('319 - controller - sudo env BITMEX_API_KEY_ID=', key, ' BITMEX_API_KEY_SECRET=', secret, ' bitmex ', sideUrl, ' ', amountUrl);
      shell.exec('sudo env BITMEX_API_KEY_ID=' + key + ' BITMEX_API_KEY_SECRET='+ secret + ' bitmex  ' + sideUrl + ' ' + amountUrl).then(r => res.json(r)); 
      if (sideUrl === 'Buy') {
        //updateLongs(amountUrl);
        var t1 = parseFloat(amountUrl) + longAmount;

        if (parseFloat(shortAmount) >= 0) {
          console.log('shorts exist');
          var t2 = shortAmount - parseFloat(amountUrl);

          //if (t2 <0){                t2 = 0;              }
          console.log(shortAmount, parseFloat(amountUrl), t2);
          console.log('set shorts');
          localStorage.setItem('shorts', t2)
        }
        localStorage.setItem('longs', t1);
      }

      if (sideUrl === 'Sell') {
        //updateShorts(amountUrl);
        var t1 = amountUrl + shortAmount;

        if (parseFloat(longAmount) >= 0) {
          console.log('longs exist');
          var t2 = parseFloat(longAmount) - amountUrl;
          if (t2 < 0) {
            t2 = 0;
          }
         // console.log(parseFloat(longAmount), amountUrl, t2);
          console.log('set longs');
          localStorage.setItem('longs', t2)
        }
        localStorage.setItem('shorts', t1);
      }

     // console.log("adaptiveLimit - " + longAmount, t1, t2, shortAmount, amountUrl);

      res.send({
        apiKey
      });
    } catch (e) {
      console.error(e);
    };
  }());

    }
  }

  pullRepo(req, res){
    console.log('adaptReversal -----------')
    var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var symbolUrl = req.query.symbol;
  var sideUrl='';
var fs = require('fs');
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var signal = localStorage.getItem("latestSignal")
  var amountUrl = req.query.amount;

  (async function main() {

    try {
      const shell = require('shelljs')
      if(process.env.DOMAIN === 'localhost' ){
        console.log('push');
        shell.exec(' push.bat ').then(r => res.json(r)); 
      
        console.log("push.bat - " + longAmount, t1, t2, shortAmount, amountUrl);

        res.send({
          apiKey
        });
      }
      if(process.env.DOMAIN === 'mrgreenswag.ddns.net' ){
        console.log('sh ./update.sh');  //thsdfsdadingte
        shell.exec(' sudo sh ./update.sh').then(r => res.json(r)); 
      
       // console.log("adaptiveLimit - " + longAmount, t1, t2, shortAmount, amountUrl);

        res.send({
          apiKey
        });
      }
    } catch (e) {
      console.error(e);
    };
  }());

  }
  initCouchDB(){
    var nano = require('nano')('http://localhost:5984');


// clean up the database we created previously
nano.db.destroy('alice', function() {
  // create a new database
  nano.db.create('alice', function() {
    // specify the database we are going to use
    var alice = nano.use('alice');
    // and insert a document in it
    alice.insert({ crazy: true }, 'rabbit', function(err, body, header) {
      if (err) {
        console.log('[alice.insert] ', err.message);
        return(200);
      }
      console.log('you have inserted the rabbit.')
      console.log(body);
      return(200)
    });
  });
});
  }
  

}


function makeRequest(apiKeyPassed, apiSecretPassed, verb, endpoint, data = {}) {

  var verb = 'POST';
  let path = '/api/v1/' + endpoint;
  let expires = Math.round(new Date().getTime() / 1000) + 60;
  var postBody = JSON.stringify(data);

  var signature = crypto.createHmac('sha256', apiSecretTrade).update(verb + path + expires + postBody).digest('hex');

  //console.log(apiKeyPassed, apiSecretPassed, verb, endpoint, data);

  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKeyTrade,

    'api-signature': signature
  };

  const requestOptions = {
    headers: headers,
    url: 'https://www.bitmex.com' + path,
    method: verb,
    body: postBody
  };

  //console.log(requestOptions);

  request(requestOptions, function (error, response, body) {
    if (error) {
      console.log(error);
    }

    pos = JSON.parse(body);
   // console.log(body);
    var string = JSON.stringify(pos);
    var objectValue = JSON.parse(string);
    console.log(objectValue.orderID);
    currentOrderId = objectValue.orderID;
   
    localStorage.setItem("currentOrderId", currentOrderId)
 
  console.log('currentOrderId IS:', currentOrderId )

    return currentOrderId;


  });
};
function getPosition(apiKeyPassed, apiSecretPassed, verb, endpoint, data = {}) {

  var verb = 'GET';
  let path = '/api/v1' + endpoint;
  let expires = Math.round(new Date().getTime() / 1000) + 60;
  var postBody = JSON.stringify(data);

  var signature = crypto.createHmac('sha256', apiSecretTrade).update(verb + path + expires + postBody).digest('hex');

 //console.log(apiKeyPassed, apiSecretPassed, verb, endpoint, data);

  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKeyTrade,

    'api-signature': signature
  };

  const requestOptions = {
    headers: headers,
    url: 'https://www.bitmex.com' + path,
    method: verb,
    body: postBody
  };

 // console.log(requestOptions);

 request(requestOptions, function (error, response, body) {
  if (error) {
    console.log(error);
  }
  l.info(`${this.constructor.name}`, body.res)
  pos = JSON.parse(body);
 
  var string = JSON.stringify(pos);
  var objectValue = JSON.parse(string);
  currentQty = objectValue.currentQty;
  //localStorage.setItem("currentQty",  currentQty)

//console.log(' CURRENTQTY IS:',currentQty)

  return currentQty;

//    return pos;
});
};
function cancelOrders(apiKeyPassed, apiSecretPassed, verb, endpoint, data = {}) {

  var verb = 'POST';
  let path = '/api/v1/' + endpoint;
  let expires = Math.round(new Date().getTime() / 1000) + 60;
  var postBody = JSON.stringify(data);

  var signature = crypto.createHmac('sha256', apiSecretTrade).update(verb + path + expires + postBody).digest('hex');

 // console.log(apiKeyPassed, apiSecretPassed, verb, endpoint, data);

  var headers = {
    'content-type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    // This example uses the 'expires' scheme. You can also use the 'nonce' scheme. See
    // https://www.bitmex.com/app/apiKeysUsage for more details.
    'api-expires': expires,
    'api-key': apiKeyTrade,

    'api-signature': signature
  };

  const requestOptions = {
    headers: headers,
    url: 'https://www.bitmex.com' + path,
    method: verb,
    body: postBody
  };

 // console.log(requestOptions);

  request(requestOptions, function (error, response, body) {
    if (error) {
      console.log(error);
    }
//console.log(body);
 
  console.log(' ORDERS CANCELLING')

    return 'OK';

//    return pos;
  });
};
function adapt(apiKeyPassed, apiSecretPassed, verb, endpoint, data = {}) {
console.log('ADAPT ---------------')
  var fs = require('fs');
  var apiKey = req.query.apiKey;
  var apiSecret = req.query.apiSecret;
  var sideUrl = req.query.side;
  var amountUrl = req.query.amount;

  (async function main() {
    try {
      const shell = require('shelljs');

     /* console.log(
        'env BITMEX_API_KEY_ID=' +
        apiKey +
        ' BITMEX_API_KEY_SECRET=' +
        apiSecret + '  ' +
        //          ' node index.js ' +
        ' bitmex ' +
        sideUrl +
        ' ' +
        amountUrl

      );
*/
      shell.exec(
        'env BITMEX_API_KEY_ID=' +
        apiKey +
        ' BITMEX_API_KEY_SECRET=' +
        apiSecret + '  ' +
        //          ' node index.js ' +
        '  bitmex ' +
        sideUrl +
        ' ' +
        amountUrl
      ); console.log('resetscripts - - - - ');

      res.send({
        apiKey
      });
    } catch (e) {
      console.error(' ADAPTIVE LIMIT ERROR: ' + e);
      cancelOrders();
    }
  })();

};


export default new Controller();