import * as express from 'express';
import controller from './controller';

export default express
  .Router()
  //.post('/', controller.create)
  .get('/', controller.all)
  .get('/nu/:val', controller.addTodo)
  .get('/pullRepo', controller.pullRepo)
  .post('/pullRepo', controller.pullRepo) ///lklklk
  //for using trend to determine which side to place stop loss on, thinking its beter to use the long and short start values then one singulair direction variable that could fault.. or maybe both. commenting out next line.
  //.get('/guardDirection/:guardDirection', controller.setGuardDirection )
  .get('/midPrice/:midPriceValue', controller.addGuardValue)
  //testing node-bitmex
  .get('/test/instrumentDev', controller.instrumentDev)  
  .get('/placeOrder', controller.placeOrder)
  .post('/placeOrder', controller.placeOrder)
  .get('/getPositionInfo', controller.getPositionInfo)
  .get('/getOrders', controller.getOrders)
  .post('/getOrders', controller.getOrders)
  .get('/cancelOrders', controller.cancelOrders) 
  .post('/cancelOrders', controller.cancelOrders) 
  .post('/adaptReversal', controller.adaptReversal) 
  .post('/updatePrimaryOrder', controller.updatePrimaryOrder) 
  //short stop
  .get('/getSsl', controller.getShortStopLoss)
  .get('/shortStop/', controller.shortStop)
  .get('/sslUpdate/', controller.shortStopLossUpdate)

  //long stop
  .get('/getLsl', controller.getLongStopLoss)
  .get('/lslStart/', controller.longStopLossStart)
  .get('/lslUpdate/', controller.longStopLossUpdate)

  //updates to db
  .get('/supertrend/update', controller.updateSuperTrendInfo)
  .post('/supertrend/update', controller.updateSuperTrendInfo)
  /* .get('/supertrend/limit/closeLongReversal', controller.closeLongReversal)
  .get('/supertrend/limit/closeShortReversal', controller.closeShortReversal)
  .get('/supertrend/limit/openReversalPosition', controller.openReversalPosition)
  .get('/supertrend/limit/openReversalPosition', controller.closeReversalPosition)*/

  .post('/set/signal/buy', controller.setSignalLong)
  .post('/set/signal/sell', controller.setSignalShort)

 //.post('/set/wolf/buy', controller.placeWolfLong)
  //.post('/set/wolf/sell', controller.placeWolfShort)
  
  .get('/set/limit/longs/:limitLongs', controller.setLimitLongs)
  .get('/set/limit/shorts/:limitShorts', controller.setLimitShorts)
  .get('/set/limit/reversal/:limitReversal', controller.setLimitReversal)

  .get('/confirmDirection/bull', controller.updateSuperTrendInfo)
  .get('/confirmDirection/bear', controller.updateSuperTrendInfo)
  
  .get('/wolf/adapt', controller.wolfPlaceOrder)



  //read from db
  .get('/pos/held/goal', controller.updateSuperTrendInfo)

  //read from db
  .get('/initCouchDB', controller.initCouchDB)

  