var request = require('request');
const bodyParser = require('body-parser');

//from previous mr-green-build
const crypto = require('crypto');
const qs = require('qs');
const fetch = require("node-fetch");
const express = require('express');;
const app = express();
const port = process.env.PORT || 3000;
const axios = require("axios");

var cors = require('cors');
var fs = require('fs-extra');
let unix_timestamp = 0;
let formattedTime =0;

var throttledQueue = require('throttled-queue');

var closingIndicatorTimer = require('../server/mr-greens-scripts/closingIndicatorTimer.js');



global.requests_per_interval = 1;
        global.interval = 1200;
        global.throttle = throttledQueue(requests_per_interval, interval, true);
global.time_started = Date.now();
        global.max_rpms = requests_per_interval / interval;
global.num_requests = 20;
global.request_limit = 100;
global.userRequests = 0;

global.seconds = 0;
global.secondsReset = false; 
global.secondsSwitch = false;


 
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


export default class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    app.set('appPath', `${root}client`);
    app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(
      bodyParser.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || '100kb',
      })
    );
    app.use(bodyParser.text({ limit: process.env.REQUEST_LIMIT || '100kb' }));
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(Express.static(`${root}/public`));

    const apiSpec = path.join(__dirname, 'openapi.yaml');
    const validateResponses = !!(
      process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION &&
      process.env.OPENAPI_ENABLE_RESPONSE_VALIDATION.toLowerCase() === 'true'
    );
    app.use(process.env.OPENAPI_SPEC || '/spec', Express.static(apiSpec));
    new OpenApiValidator({
      apiSpec,
      validateResponses,
    }).install(app);
  }

  router(routes) {
    routes(app);
    app.use(errorHandler);

    return this;
  }

  listen(port = process.env.PORT) {
    const welcome = p => () =>
      l.info(
        `up and running in ${process.env.NODE_ENV ||
          'development'} @: ${os.hostname()} on port: ${p}}`
      );
    http.createServer(app).listen(port, welcome(port));
    return app;
  }
}









