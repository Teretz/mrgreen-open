import examplesRouter from './api/controllers/examples/router';
import mrgreenRouter from './api/controllers/mrgreen/router';


export default function routes(app) {
  app.use('/api/v1/examples', examplesRouter);
//  app.use('/api/livenet', livenetRouter);
  
  app.use('/api/mrgreen', mrgreenRouter);

  

}
