made with: <br/>
https://linuxize.com/post/how-to-install-couchdb-on-ubuntu-18-04/
https://github.com/cdimascio/generator-express-no-stress<br/>
https://bitbucket.org/Teretz/mr-greenswag/src/master/<br/>
https://github.com/pouchdb/pouchdb-server<br/>
https://pouchdb.com/getting-started.html<br/><br/>
Firecamp chrome extension

considered using or was inspired by:
https://gist.github.com/SinanGabel/eac83a2f9d0ac64e2c9d4bd936be9313#file-couchdb-2-0-install-L39
https://github.com/manifoldco/definitely-not-a-todo-list

reminders:
http://www.callstack.in/tech/blog/windows-kill-process-by-port-number-157

SNIPPETS:
sudo kill -9 $(sudo lsof -t -i:3000)


APPROACH:
Using Ubuntu 18.04 as the OS of choice for the online servers in AWS, grab a new instance server, start the build by installing Apache CouchDB (https://www.howtoforge.com/tutorial/ubuntu-couchdb/), and then add in the bitbucket.org/teretz/mr-greenswag repo by cloning into an empty directory and just cp the result over the fresh AWS CouchDB instance. Thesecond time I worked on this prototype and had to rebuild it on another server, I had difficulties at the end if i were to clone the mr-greenswag repo first and then put couchdb on top of it. 


# myapp

My cool app

## Get Started

Get started developing...

```shell
# install deps
npm install

# run in development mode
npm run dev

# run tests
npm run test
```

## Install Dependencies

Install all package dependencies (one time operation)

```shell
npm install
```

## Run It
#### Run in *development* mode:
Runs the application is development mode. Should not be used in production

```shell
npm run dev
```

or debug it

```shell
npm run dev:debug
```

#### Run in *production* mode:

Compiles the application and starts it in production production mode.

```shell
npm run compile
npm start
```

## Test It

Run the Mocha unit tests

```shell
npm test
```

or debug them

```shell
npm run test:debug
```

## Try It
* Open you're browser to [http://localhost:3000](http://localhost:3000)
* Invoke the `/examples` endpoint 
  ```shell
  curl http://localhost:3000/api/v1/examples
  ```


## Debug It

#### Debug the server:

```
npm run dev:debug
```

#### Debug Tests

```
npm run test:debug
```

#### Debug with VSCode

Add these [contents](https://github.com/cdimascio/generator-express-no-stress/blob/next/assets/.vscode/launch.json) to your `.vscode/launch.json` file
## Lint It

View prettier linter output

```
npm run lint
```

Fix all prettier linter errors

```
npm run lint
```

## Deploy It

Deploy to CloudFoundry

```shell
cf push myapp
```


   
